AC_DEFUN([smr_WITH_BUILD_PATH],
[
    AC_ARG_WITH([build-path],
    [  --with-build-path=DIR   build using DIR/include and DIR/lib],
    [
        for d in `echo $withval | tr : ' '`; do
            test -d $d/include && CPPFLAGS="$CPPFLAGS -I$d/include"
            test -d $d/lib && LDFLAGS="$LDFLAGS -L$d/lib"
        done
    ])
])

