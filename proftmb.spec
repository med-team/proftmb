Summary: per-residue prediction of bacterial transmembrane beta barrels
Name: proftmb
Version: 1.1.7
Release: 1
License: GPL-3+
Group: Applications/Science
Source: ftp://rostlab.org/%{name}/%{name}-%{version}.tar.gz
URL: http://rostlab.org/
BuildRoot: %{_tmppath}/%{name}-%{version}-root
BuildRequires: help2man, gsl-devel, opt
Requires: pp-popularity-contest

%description
 proftmb provides a four state (up-strand, down-strand, periplasmic loop, and outer loop) per-residue prediction for the protein.
 On a jackknifed test set of 8 families of TMBs of known structure, our method predicted 87.2% of residues correctly.

%prep
%setup -q

%build
%configure
make

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=${RPM_BUILD_ROOT} install
rm -rf ${RPM_BUILD_ROOT}/%{_docdir}/%{name}/README

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS
%doc COPYING
%doc README
%{_bindir}/proftmb
%{_mandir}/*/*
%{_datadir}/%{name}/*

%changelog
* Tue Sep 13 2011 Laszlo Kajan <lkajan@rostlab.org> - 1.1.7-1
- First rpm package
