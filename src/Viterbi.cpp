#include "Seq.h"
#include "Par.h"
#include <iostream>
#include <cmath>


void Seq::Viterbi(Par& M){

	scl.P_free_log = Forward(M);
	Backward(M);

	this->ViterbiInit(M);
	this->ViterbiInduction(M);
	this->ViterbiTermination(M);
	this->ViterbiTraceback(M);

 	this->CountPredStrands();
	if (scl.labelQ) this->CalcCTable(M);

}


void Seq::ViterbiInit(Par& M){
	//initialization
	unsigned int t, T = this->scl.Seqlen, NA = Par::NumA;
	unsigned int ian, n;
	double aij_log;

	for (t = 0; t < T; t++)
		for (ian = 0;ian<NA;ian++)
			trel[t][ian].cs_log =- HUGE_VAL;
	
	for (n = 0; n<M.ArchSize[Par::Begin_ian].ntar; n++){
		ian  =  M.Arch[Par::Begin_ian][n].node;
		aij_log = log(M.Arch[Par::Begin_ian][n].score);
		if (ian == Par::Begin_ian ||
			ian == Par::End_ian) continue;

		trel[0][ian].cs_log = aij_log + log((this->*this->pReturn)(0,ian,M));
	}
}


void Seq::ViterbiInduction(Par& M){

	unsigned int t, T = this->scl.Seqlen, NA = Par::NumA;
	unsigned int s_ian, t_ian, n, fn;
	double aij_log, tmpcs_log;

	for (t = 1;t<T;t++) {
		for (t_ian = 0;t_ian<NA;t_ian++){
			for (n = 0;n<M.ArchSize[t_ian].nsrc;n++) {
				s_ian = M.ArchRev[t_ian][n].src;
				
				if (s_ian == Par::Begin_ian ||
					s_ian == Par::End_ian) continue;

				fn = M.ArchRev[t_ian][n].index;
				aij_log = log(M.Arch[s_ian][fn].score);

				tmpcs_log = trel[t-1][s_ian].cs_log
					+ aij_log
					+ log((this->*this->pReturn)(t,t_ian,M));

				//if (trel[t][t_ian].cs_log == tmpcs_log) 
				//	cout<<"Warning: viterbi found same cs_log at position "<<t<<endl;

				if (trel[t][t_ian].cs_log<tmpcs_log) {
					trel[t][t_ian].cs_log = tmpcs_log;
					trel[t][t_ian].s_atom = s_ian;
				}
			}
		}
	}

}


void Seq::ViterbiTermination(Par& M){
	
	unsigned int ian, n, T = scl.Seqlen;
	double aij;
	set<unsigned int> node, nodes;
	
	for (ian = 0; ian < Par::NumA; ian++) nodes.insert(ian);
	vector<pair<unsigned int, double> >sources = M.FindSources(Par::End_ian, nodes);

	for (n = 0; n < sources.size(); n++){
		ian = sources[n].first;
		aij = sources[n].second;
		trel[T-1][ian].cs_log += log(aij); //should strictly use a different variable
	}

}


void Seq::ViterbiTraceback(Par& M){

	unsigned int ian, n;
	unsigned int T = this->scl.Seqlen;
	unsigned int cur_ian, maxi = 0;
	double max_log=-HUGE_VAL;

	set<unsigned int> nodes;

	//initialization: find top-scoring last atom
	for (ian = 0; ian < Par::NumA; ian++) nodes.insert(ian);
	vector<pair<unsigned int, double> >sources = M.FindSources(Par::End_ian, nodes);

	for (n = 0; n < sources.size(); n++){
		ian = sources[n].first;

		if (trel[T-1][ian].cs_log == max_log)
			cout<<"Warning: found equal cs_log in viterbi at last position "<<T-1<<endl;
		if (trel[T-1][ian].cs_log > max_log) {
			max_log = trel[T-1][ian].cs_log;
			maxi = ian;
		}
	}
	
	//initialize 
	cur_ian = maxi;

	//trace back by s_atom
	int t;
	for (t = T-1; t>=0; t--) {
		this->row[t].pred_ian = cur_ian;
		this->row[t].pred_rstate = Par::ReduxDecode[cur_ian];
		cur_ian = trel[t][cur_ian].s_atom;
	}
}
