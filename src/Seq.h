#ifndef _SEQ
#define _SEQ

#include "structs.h" //for small structures like proftriple
#include <cmath>
#include <vector>
#include <map>
#include <string>
#include <set>
#include <utility>
#include <fstream>

using std::vector;
using std::string;
using std::set;
using std::map;
using std::pair;
using std::ifstream;

class Par; //Forward declaration


struct vec {
	unsigned int iaa;
	unsigned int act_ian, pred_ian;
	unsigned int act_rstate, pred_rstate; //reduced state from ReduxDecode
	string cpn, cln;
	double fbs; 
	double b,c,n,C_log,D_log,N_path_log;
	unsigned int N_ext;
	bool predbeta_atomQ,predbetaQ,actbetaQ;
	//c(t)=auxiliary scale factor =sum_i(alpha_auxiliary(i,t))
	//C=forward scale factor =c(0)*c(1)*...*c(t)
	//D=backward scale factor = c(T-1)*c(T-2)*...*c(t)
	//labeled state signature
	//forward-backward-sum, an intermediate variable used in the calculation
	//N_path_log=log of # paths

	void init_vec(){
		fbs=b=c=n=0.0;
		C_log=D_log=N_path_log=-HUGE_VAL;
		N_ext=0;
	}
	vec():fbs(0),b(0),c(0),n(0),C_log(-HUGE_VAL),
		 D_log(-HUGE_VAL),N_path_log(-HUGE_VAL),N_ext(0){}
	//warning: calling vec() seems to initialize other things
	//like actbetaQ that we don't want initialized
};


class Seq {
 public:
	static struct labels {string id,lb,sq,ts,wt,pr,H,E,L;} lb;
	struct SeqData {
		unsigned int Seqlen;
		string SeqID,AASeq,Set,ClassDesc,ReducedDesc; //dataset the sequence is from
		double P_clamp_log,P_free_log,P_log, RV_log,P_log_ave;
		double P_scl_clamp, P_scl_free,	C_log, P_log_vp;
		double P_log_ap,P_scl_h,Z_log,S,Pnorm_log,null_log;
		double Pbits, Score, RVbits, N_path_log, evalue, pvalue;
		float weight; //whole-sequence weighting for redundancy reduction
		//used in Par::ComputeNormE (Baum Welch updating)

		//Pbits=P_log - null_log
		//RVbits=RV_log - null_log
		//ap 'all path' vp, 'valid path'
		//S is sum of profile column
		//RVbits is the reduced viterbi bits score = 
		bool posQ; //true if this sequence is in the positive set
		bool labelQ; //true if sequence has labelling
		int npredstrands;//number of predicted beta-strands for this
		//sequence
	} scl;

	vector<vec>row;
	vector<unsigned int>Label; //Label[t] = valid iln
	vector<vector<float> >Profile; //Profile[t][c]=double
	vector<vector<mat> >trel; //trel[t][i]=double
	vector<vector<int> >CTable; //CTable[pred][act]=count
	static string CheckDat(set<pair<string,string> >&);
	static string CheckProfile(set<pair<string,string> >&,unsigned int);
	//void IncrAtom(double (*) (unsigned int,unsigned int));
	void InitProfile(set<pair<string,string> >&,unsigned int);
	void InitProfile(const char*);
	bool InitLabels(set<pair<string,string> >&) throw (string&);

	//from Posterior.cpp
	void CountPredStrands();
	void Posterior(Par&);
	vector<vector<double> > CalcReduxPosterior(Par&);
	
	void LogViterbiMulti(Par&);
	void PosteriorTraceback(Par&);

	//from Viterbi.cpp
	void Viterbi(Par&);
	void ViterbiInit(Par&);
	void ViterbiInduction(Par&);
	void ViterbiTermination(Par&);
	void ViterbiTraceback(Par&);

	//from Forward.cpp
	double Forward(Par&, vector<set<unsigned int> >&);
	double Forward(Par&);

	void ForwardAlloc(vector<vector<mat> >&, vector<vec>&);
	void ForwardClear(vector<vector<mat> >&, vector<vec>&);

	void ForwardInit(Par&,
					 vector<mat>&,
					 vec&,
					 set<unsigned int>&);
	
	void ForwardInduct(Par&, 
					   unsigned int,
					   const vector<mat>&,
					   vector<vec>&,
					   const vec&,
					   vec&,
					   const set<unsigned int>&,
					   const set<unsigned int>&);

	double Term(Par&,
				   const vector<mat>&,
				   const vec&,
				   const set<unsigned int>&);

	void CalcFinal(Par&);
	void CalcDlog(vector<vec>&);
	
	double AAux(Par&,
				const unsigned int,
				const unsigned int,
				const vector<mat>&,
				const set<unsigned int>&,
				const set<unsigned int>&);

	void AScale(Par&,
				vector<mat>&,
				double,
				vec&,
				const set<unsigned int>&);
	
	double CalcNulllog(Par&);
	
	//from Backward.cpp
	void Backward(Par&, vector<set<unsigned int> >&);
	void Backward(Par&);
	void BackwardInit(Par&,
					  vector<mat>&,
					  vec&,
					  set<unsigned int>&);
	
	void BackwardInduct(Par&, 
					   unsigned int,
					   const vector<mat>&,
					   vector<vec>&,
					   const vec&,
					   vec&,
					   const set<unsigned int>&,
					   const set<unsigned int>&);
	
	double BAux(Par&,
				const unsigned int,
				const unsigned int,
				const vector<mat>&,
				const set<unsigned int>&,
				const set<unsigned int>&);

	void BScale(vector<mat>&,
				vec&,
				const set<unsigned int>&);
		
	//from Krogh1Best.cpp
	void Krogh1Best(Par&);

	//from ???
	void TallyStates(unsigned int,Par&);
	void CalcCTable(Par&);

	static struct ext {
		enum {A,P,AGProf,ALProf,PGProf,PLProf,Null}; //these are hard-coded names to correspond with
		//functions ReturnA, ReturnP, etc. in class Seq
	} func;
	Seq();
	Seq(const char*,const char*);
	Seq(set<pair<string,string> >&,string,
		set<string>&,map<string,string>&);
	double (Seq::*pReturn)(unsigned int,unsigned int,Par&); //decides which return function to be used
	double ReturnA(unsigned int,unsigned int,Par&);
	double ReturnP(unsigned int,unsigned int,Par&);

	static void InitSeq();
	static map<string,set<pair<string,string> > > Read(char*) throw (string&);
	static pair<string,set<pair<string,string> > > ReadOne(ifstream&);
};

#endif
