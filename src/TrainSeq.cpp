#include "TrainSeq.h"

TrainSeq::TrainSeq (){}

TrainSeq::TrainSeq (set<pair<string,string> >&sdat,string id,
					set<string>& emptyset,map<string,string>& nomap)
	: Seq(sdat,id,emptyset,nomap){
}	


void TrainSeq::Allocate(Par& M){

	A_clamp.resize(Par::NumATotal);
	A_free.resize(Par::NumATotal);
	C_clamp.resize(Par::NumE);
	C_free.resize(Par::NumE);

	unsigned int ian, ien;
	for (ian=0; ian < Par::NumATotal; ian++) {
		A_clamp[ian].resize(M.ArchSize[ian].ntar);
		A_free[ian].resize(M.ArchSize[ian].ntar);
	}
	for (ien=0; ien<Par::NumE; ien++) {
		C_clamp[ien].resize(Par::NUMAMINO);
		C_free[ien].resize(Par::NUMAMINO);
	}
}
