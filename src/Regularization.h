#ifndef _Regularization
#define _Regularization

#include <vector>

using std::vector;

struct dirichlet {
	vector<double> coeff;
	vector<vector<double> > alpha;
};

double NgivenAlpha(vector<double>&, vector<double>&);
vector<double> Dirichlet(vector<double>&, dirichlet&);
void ArchPrior(vector<double>&);
void EmitPrior(vector<double>&);

#endif
