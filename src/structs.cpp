#include "structs.h"
#include "Par.h"
#include <sstream>
#include <iomanip>
#include <cmath>

using namespace std;

ostream& operator<<(ostream& out,names& n){
	out<<"iln="<<n.iln<<", ian="<<n.ian;
	out<<", ien="<<n.ien<<", sln="<<n.sln;
	out<<", san="<<n.san<<", cln="<<n.cln;
	out<<", betaQ="<<n.betaQ;
	return out;
}


ostream& operator<<(ostream& out,evaldat& ev){
	string twoclass;
	ev.posQ ? twoclass="positive" : twoclass="negative";
	out<<ev.cl<<'\t'<<twoclass<<'\t'<<ev.id
	   <<'\t'<<ev.length;
	return out;
}


ostream& operator<<(ostream& out,triplet& tr){
	out<<tr.cl<<'\t'<<tr.sco<<'\t'<<tr.cov;
	return out;
}


triplet::triplet(string c,double s,double co):
	cl(c),sco(s),cov(co){}


measure::measure(){
	meanQpred.resize(Par::NEval);
	sdQpred.resize(Par::NEval);
	meanQobs.resize(Par::NEval);
	sdQobs.resize(Par::NEval);
	meanMCC.resize(Par::NEval);
	sdMCC.resize(Par::NEval);
}


string measure::PrintHeader(vector<measure>& M){

	ostringstream out;
	unsigned int state;
	char olc;
	unsigned int i, N = M.size();

	for (i=0; i < N; i++) out<<M[i].pfx<<"_Q\t";
	for (i=0; i < N; i++) out<<M[i].pfx<<"_SovPos\t";
	for (i=0; i < N; i++) out<<M[i].pfx<<"_SovAll\t";
	for (state = 0; state < M[0].meanQpred.size(); state++){
		olc = Par::Evalrev[state][0];
		for (i=0; i < N; i++) out<<'\t'<<M[i].pfx<<"_Qpred_"<<olc;
		for (i=0; i < N; i++) out<<'\t'<<M[i].pfx<<"_Qobs_"<<olc;
		for (i=0; i < N; i++) out<<'\t'<<M[i].pfx<<"_MCC_"<<olc;
	}
	
	return out.str();
}


string measure::PrintData(vector<measure>& M){

	ostringstream out;
	out.precision(1);
	out.width(2);
	out.setf(ios::fixed);

	unsigned int state;
	unsigned int i, N = M.size();

	for (i=0; i < N; i++) out<<M[i].meanQ<<'\t';
	for (i=0; i < N; i++) out<<M[i].meanSOVPos<<'\t';
	for (i=0; i < N; i++) out<<M[i].meanSOVALL<<'\t';
	for (state = 0; state < M[0].meanQpred.size(); state++){
		for (i=0; i < N; i++) out<<'\t'<<M[i].meanQpred[state];
		for (i=0; i < N; i++) out<<'\t'<<M[i].meanQobs[state];
		for (i=0; i < N; i++) out<<'\t'<<M[i].meanMCC[state];
	}
	
	return out.str();
}


ostream& operator<<(ostream& out, measure& m){

	if (m.printSD)
		out<<m.meanQ<<"\t"<<m.sdQ<<"\t"<<m.meanSOVPos<<"\t"
		   <<m.sdSOVPos<<"\t"<<m.meanSOVALL<<"\t"<<m.sdSOVALL;
	else 
		out<<m.meanQ<<"\t"<<m.meanSOVPos<<"\t"<<m.meanSOVALL;


	unsigned int state;
	if (m.printSD)
		for (state = 0; state < m.meanQpred.size(); state++){
			out<<"\t"<<m.meanQpred[state]<<"\t"<<m.sdQpred[state]
			   <<"\t"<<m.meanQobs[state]<<"\t"<<m.sdQobs[state]
			   <<"\t"<<m.meanMCC[state]<<"\t"<<m.sdMCC[state];
		}
	
	else 
		for (state = 0; state < m.meanQpred.size(); state++){
			out<<"\t"<<m.meanQpred[state]
			   <<"\t"<<m.meanQobs[state]
			   <<"\t"<<m.meanMCC[state];
		}

	return out;
}
