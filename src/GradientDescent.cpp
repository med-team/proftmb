#include "Par.h"
#include "TrainSeq.h"
#include "HTools.h"
#include "Tools.h"
#include <algorithm>
#include <iterator>

using namespace std;


//[1]: P. Bagos et al "Faster Gradient Descent Training of Hidden Markov Models"

/*
for each k { //state k
  for each a { //symbol a
    if (dL(t)/dz_ka * dL(t-1)/dz_ka > 0) { del_ka(t) = min(del_ka(t-1) * a_+, del_max) }
    else if (dL(t)/dz_ka * dL(t-1)/dz_ka < 0) { del_ka(t) = max(del_ka(t-1) * a_-, del_min); dL(t)/dz_ka = 0 }

    phi_ka(t+1) = phi_ka(t) exp[-sign(dL(t)/dz_ka) * del_ka(t)]/ sum over a'[phi_ka'(t) exp[-sign[dL(t)/dz_ka']*del_ka'(t)]]

  }
}


Notes:

0) This algorithm maximizes the quantity L by using it's first derivatives.  In this
   application, L = -log P(y|x,theta)
1) This algorithm is the same for iterating over (k,a) as for (k,j), the transitions
2) The actual values of z_ka or z_kj are never computed, only the derivatives of l w.r.t z_ka
3) The idea is that if the auxiliary variable z_ka is changing direction, as indicated
   by the sign of the derivative changing from one timestep to the next, then you will
   decrease the step size for this parameter

   If z_ka is moving in the same direction, increase the step size for the parameter

   Regardless of step size, the next value of the model parameter is calculated using
   the step size determined, and whether it is stepping up or down is determined by
   the sign of the most recent derivative.

4) The gradients dL/dz_ka are computed directly from expectations of the
   parameters:

  dL/dz_jk = -[m_jk - n_jk - theta_jk * sum over j' (m_jk' - n_jk')] (2.20) [2]
  dL/dz_ka = -[m_ka - n_ka - theta_ka * sum over j' (m_ka' - n_ka')] (2.20) [2]

  where 

  m_jk = sum_l [ sum_pi valid | (l-1,j)->(l,k) [ P(pi|x,theta) ] ]
  n_jk = sum_l [ sum_pi       | (l-1,j)->(l,k) [ P(pi|x,theta) ] ]

  m_ka = sum_l [ sum_pi valid | (l,k) [ P(pi|x,theta) x_la ] ]
  n_ka = sum_l [ sum_pi       | (l,k) [ P(pi|x,theta) x_la ] ]

  are the expectations of transitions from states j to k or
  emissions of symbol a from state k.
  in both cases m is the clamped expectation, taken over valid paths only,
  while n is the free expectation, taken over all possible paths

  all four quantities are calculated using forward backward algorithms.

  the emissions are appropriate for a dot-product emission function.
*/


vector<set<unsigned int> > LabelSet(TrainSeq&, bool);


void Par::Expectations(vector<TrainSeq>& tsq, set<string>& exclude, bool validQ){

	unsigned int s;
	vector<set<unsigned int> >labelset;
	double plog;

	for (s = 0; s < tsq.size(); s++) {
		if (exclude.find(tsq[s].scl.SeqID) != exclude.end()) continue;

		tsq[s].Allocate(*this);
		
		labelset = LabelSet(tsq[s], validQ);
		plog = tsq[s].Forward(*this, labelset);
		if (validQ) tsq[s].scl.P_clamp_log = plog;
		else tsq[s].scl.P_free_log = plog;

		tsq[s].Backward(*this, labelset);

		tsq[s].ExpectationA(*this, validQ);
		tsq[s].ExpectationC(*this, validQ);

	}
	
	//sum and normalize expectations over all training sequences
	this->TransExpectation(tsq, exclude, validQ);
	this->EmitExpectation(tsq, exclude, validQ);

}



void Par::CMLGradients(vector<TrainSeq>& tsq, set<string>& exclude){
	//compute gradients of -log P(y|x,theta) 

	//must call these in the order false, true
	this->Expectations(tsq, exclude, false);
	this->Expectations(tsq, exclude, true);

	unsigned int ian, ien, n, c, ntar = 0;
	double sum;
	for (ian = 0; ian < Par::NumATotal; ian++){
		ntar = this->ArchSize[ian].ntar;
		sum = 0.0;
		for (n = 0; n < ntar; n++)
			sum += this->A_clamp[ian][n] - this->A_free[ian][n];

		for (n = 0; n < ntar; n++)
			zArch[ian][n].second =
				-(this->A_clamp[ian][n] - this->A_free[ian][n] -
				  this->Arch[ian][n].score * sum);
	}

	for (ien = 0; ien < Par::NumE; ien++){
		sum = 0.0;
		for (c = 0; c < Par::NUMAMINO; c++)
			sum += this->C_clamp[ien][c] - this->C_free[ien][c];

		for (c = 0; c < Par::NUMAMINO; c++)
			zEmit[ien][c].second =
				-(this->C_clamp[ien][c] - this->C_free[ien][c] -
				  this->EmitAmino[ien][c] * sum);
	}
}	


void Par::MLFreeGradients(vector<TrainSeq>& tsq, set<string>& exclude){
	//compute gradients for -log P(x|theta)

	this->Expectations(tsq, exclude, false);

	unsigned int ian, ien, n, c, ntar = 0;
	double sum;
	for (ian = 0; ian < Par::NumATotal; ian++){
		ntar = this->ArchSize[ian].ntar;
		sum = 0.0;
		for (n = 0; n < ntar; n++)
			sum += this->A_free[ian][n];

		for (n = 0; n < ntar; n++)
			zArch[ian][n].second =
				-(this->A_free[ian][n] - this->Arch[ian][n].score * sum);
	}

	for (ien = 0; ien < Par::NumE; ien++){
		sum = 0.0;
		for (c = 0; c < Par::NUMAMINO; c++)
			sum += this->C_free[ien][c];

		for (c = 0; c < Par::NUMAMINO; c++)
			zEmit[ien][c].second =
				-(this->C_free[ien][c] - this->EmitAmino[ien][c] * sum);
	}
}	


void Par::MLClampGradients(vector<TrainSeq>& tsq, set<string>& exclude){
	//compute gradients for -log P(x,y|theta)

	this->Expectations(tsq, exclude, true);

	unsigned int ian, ien, n, c, ntar = 0;
	double sum;
	for (ian = 0; ian < Par::NumATotal; ian++){
		ntar = this->ArchSize[ian].ntar;
		sum = 0.0;
		for (n = 0; n < ntar; n++)
			sum += this->A_clamp[ian][n];

		for (n = 0; n < ntar; n++)
			zArch[ian][n].second =
				-(this->A_clamp[ian][n] - (this->Arch[ian][n].score * sum));
	}

	for (ien = 0; ien < Par::NumE; ien++){
		sum = 0.0;
		for (c = 0; c < Par::NUMAMINO; c++)
			sum += this->C_clamp[ien][c];

		for (c = 0; c < Par::NUMAMINO; c++)
			zEmit[ien][c].second =
				-(this->C_clamp[ien][c] - (this->EmitAmino[ien][c] * sum));
	}
}	

void TimeStep(STEP& step){
	//copy all values of step[x][y].second to step[x][y].first
	unsigned int x, y;
	for (x = 0; x < step.size(); x++){
		for (y = 0; y < step[x].size(); y++){
			step[x][y].first = step[x][y].second;
			step[x][y].second = 0.0;
		}
	}
}


void Par::AdvanceTime(){
	TimeStep(zArch);
	TimeStep(zEmit);
	TimeStep(delArch);
	TimeStep(delEmit);
}

 
void Par::UpdateEmit(){
	//updates Emission parameters according to Algorithm 2 of [1]
	//changes del at present time, and possibly z at second time

	unsigned int ien, c;
	double prod, sign = 0.0;
	vector<double>emiss(Par::NUMAMINO); //temporary for holding new transition parameters

	vector<vector<double> >	&Cref = 
		((Par::Objective == MLFREE) ? this->C_free : this->C_clamp);

	for (ien = 0; ien < Par::NumE; ien++){
		for (c = 0; c < Par::NUMAMINO; c++) {

			prod = zEmit[ien][c].first * zEmit[ien][c].second;
			assert(! isnan(prod) && ! isinf(prod));
			if (prod > 0.0)
				delEmit[ien][c].second = min(delEmit[ien][c].first * Par::APlus, Par::DelMax);
			else if (prod < 0.0){
				delEmit[ien][c].second = max(delEmit[ien][c].first * Par::AMinus, Par::DelMin);
				zEmit[ien][c].second = 0.0;
			}
			else delEmit[ien][c].second = Par::Del;

			if (zEmit[ien][c].second > 0.0) sign = 1.0;
			else if (zEmit[ien][c].second == 0.0) sign = 0.0;
			else if (zEmit[ien][c].second < 0.0) sign = -1.0;
			else assert(0);
			//sign = ((zEmit[ien][c].second >= 0.0) ? 1.0 : -1.0);
			//cout<<sign<<'\t'<<zEmit[ien][c].second<<'\t'<<endl;
			switch (Par::Algorithm){
			case STD_GRAD_DESCENT:
				emiss[c] = this->EmitAmino[ien][c] * exp(- Par::Del * zEmit[ien][c].second);
				break;
			case ALGORITHM_1: 
				emiss[c] = this->EmitAmino[ien][c] * exp(- delEmit[ien][c].second * zEmit[ien][c].second);
				break;
			case ALGORITHM_2:
				emiss[c] = this->EmitAmino[ien][c] * exp(-1.0 * sign * delEmit[ien][c].second);
				break;
			case BAUM_WELCH:
				emiss[c] = Cref[ien][c];
				break;
			default:
				assert(0);
				break;
			}
		}

		//Update Architecture parameters with normalized new values
		BoolNormalize(&emiss[0], Par::NUMAMINO);
		for (c = 0; c < Par::NUMAMINO; c++) this->EmitAmino[ien][c] = emiss[c];
	}

}


void Par::UpdateArch(){
	//updates Arch parameters according to Algorithm 2 of [1]
	//changes del at present time, and possibly z at second time

	unsigned int ian, n, ntar = 0;
	double prod, sign = 0.0;
	vector<double>trans; //temporary for holding new transition parameters

	vector<vector<double> >	&Aref =
		((Par::Objective == MLFREE) ? this->A_free : this->A_clamp);

	for (ian = 0; ian < Par::NumATotal; ian++){
		ntar = this->ArchSize[ian].ntar;
		trans.resize(ntar);
		for (n = 0; n < ntar; n++) {

			prod = zArch[ian][n].first * zArch[ian][n].second;
			assert(! isnan(prod) && ! isinf(prod));
			if (prod > 0.0)
				delArch[ian][n].second = min(delArch[ian][n].first * Par::APlus, Par::DelMax);
			else if (prod < 0.0){
				delArch[ian][n].second = max(delArch[ian][n].first * Par::AMinus, Par::DelMin);
				zArch[ian][n].second = 0.0;
			}
			else delArch[ian][n].second = Par::Del;

			if (zArch[ian][n].second > 0.0) sign = 1.0;
			else if (zArch[ian][n].second == 0.0) sign = 0.0;
			else if (zArch[ian][n].second < 0.0) sign = -1.0;
			else assert(0);
			//sign = ((zArch[ian][n].second >= 0.0) ? 1.0 : -1.0);
			//cout<<sign<<'\t'<<zArch[ian][n].second<<endl;
			switch (Par::Algorithm){
			case STD_GRAD_DESCENT:
				trans[n] = this->Arch[ian][n].score * exp(- Par::Del * zArch[ian][n].second);
				break;
			case ALGORITHM_1: 
				trans[n] = this->Arch[ian][n].score * exp(- delArch[ian][n].second * zArch[ian][n].second);
				break;
			case ALGORITHM_2:
				trans[n] = this->Arch[ian][n].score * exp(-1.0 * sign * delArch[ian][n].second);
				break;
			case BAUM_WELCH:
				trans[n] = Aref[ian][n];
				break;
			default:
				assert(0);
				break;
			}
		}


		//Update Architecture parameters with normalized new values
		BoolNormalize(&trans[0], ntar);
		for (n = 0; n < ntar; n++) this->Arch[ian][n].score = trans[n];
	}
}


pair<double, double> Par::Update(vector<TrainSeq>& tsq, set<string>& exclude){
	//run a single step in gradient descent

	switch(Par::Objective){
	case MLFREE:
		MLFreeGradients(tsq, exclude);
		break;
	case MLCLAMP:
		MLClampGradients(tsq, exclude);
		break;
	case CML:
	case DISCRIM:
		CMLGradients(tsq, exclude);
		break;
	default:
		assert(0);
		break;
	}
	UpdateEmit();
	UpdateArch();
	AdvanceTime();

	pair<double, double> log_prob(0.0, 0.0);
	for (unsigned int s=0; s < tsq.size(); s++) {
		if (exclude.find(tsq[s].scl.SeqID) != exclude.end()) continue;
		log_prob.first += tsq[s].scl.P_clamp_log;
		log_prob.second += tsq[s].scl.P_free_log;

	}
	return log_prob;
}


void Par::InitializeGradientDescent(){
	//Set initial values for dels and first gradients

	unsigned int ian, ien, n, c, ntar = 0;
	for (ian = 0; ian < Par::NumATotal; ian++){
		ntar = this->ArchSize[ian].ntar;

		for (n = 0; n < ntar; n++) {
			zArch[ian][n].first = zArch[ian][n].second;
			delArch[ian][n].first = Par::Del;
			delArch[ian][n].second = Par::Del;
		}
	}

	float sum;
	for (ien = 0; ien < Par::NumE; ien++){
		sum = 0.0;

		for (c = 0; c < Par::NUMAMINO; c++) {
			zEmit[ien][c].first = zEmit[ien][c].second;
			delEmit[ien][c].first = Par::Del;
			delEmit[ien][c].second = Par::Del;
		}
	}

}


vector<set<unsigned int> > LabelSet(TrainSeq& S, bool posQ){
	//creates a vector<set> of arch states valid for the labels of S
	//if posQ, returns those for the positive phase (clamped)
	//otherwise for the negative phase (unclamped or complement)
	
	unsigned int ian, t, T = S.scl.Seqlen;
	vector<set<unsigned int> > labelset(T);

	switch (Par::Objective){
	case CML:
	case MLFREE:
	case MLCLAMP:
		for (t = 0; t < T; t++)
			for (ian = 0; ian < Par::NumA; ian++)
				if (TrelQ(t, ian, S, posQ)) labelset[t].insert(ian);
		break;
		
	case DISCRIM:
		for (t = 0; t < T; t++)
			for (ian = 0; ian < Par::NumA; ian++){
				if (TrelQ(t, ian, S, true) == posQ) labelset[t].insert(ian);
			}
		
		break;
	default:
		cout<<"Error:  unknown objective function Par::Objective="
			<<Par::Objective<<endl;
		exit(1);
		break;
	}

	/*
	if (posQ){
		cout<<"posQ = "<<posQ<<"----------------"<<endl;
		for (t = 0; t < T; t++){
			copy(labelset[t].begin(), labelset[t].end(), ostream_iterator<unsigned int>(cout," "));
			cout<<endl<<endl;
		}
	}
	*/

	//cout<<"posQ="<<posQ<<", labelset[0].size()="<<labelset[0].size()<<endl;
	return labelset;
}


double Par::CalcDelta(pair<double, double>& cur, pair<double, double>& prev){

	double del = 0.0;

	switch(Par::Objective){
	case MLCLAMP:
		del = cur.first - prev.first;
		break;
	case MLFREE:
		del = cur.second - prev.second;
		break;
	case CML:
		del = cur.first - cur.second - (prev.first - prev.second);
		break;
	case DISCRIM:
		del = cur.first - cur.second - (prev.first - prev.second);
		break;
	}
	return del;

}
