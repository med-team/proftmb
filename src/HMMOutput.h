#ifndef _HMMOutput
#define _HMMOutput

#include "Seq.h"
#include "Par.h"

void PrintID(Seq&);
void PrintScore(ostream&,Seq&);
void PrintPaths(ostream&,Seq&);
void PrintLabel(ostream&,Seq&);
void PrintPred(ostream&,Seq&);
void PrintPred2(ostream&,Seq&);
void Print(ostream&,Seq&,bool,bool);
string RdbHeader(bool);
void PrintRdb(ostream&,Seq&,bool, bool);
string Header(pair<double, double>&, Seq&, double, int);
void PrintPretty(ostream&,Seq&,double, int);
void PrintPosteriorDat(ostream&, Seq&, Par&, double);
void PrintArch(ostream&,Par&);
void PrintTrans(ostream&,Par&);
void DisplayArch(ostream&,Par&,uint,char* ="");
void PrintEmit(ostream&,Par&);
void PrintEmitLogOdds(ostream&,Par&,float);
void PrintEmit(ostream&,Par&,uint,char* ="");

#endif
