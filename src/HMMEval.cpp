#include "HMMEval.h"
#include "Sov.h"
#include "Tools.h"

using namespace std;

Stats EvalPred(vector<TrainSeq>& pred,bool atomQ){
	vector<Seq> spred(pred.size());
	for (uint i=0;i<pred.size();i++)
		spred[i]=pred[i];
	return EvalPred(spred,atomQ);
}


Stats EvalPred(vector<Seq>& pred,bool atomQ){
	//evaluates the p,o,u,n for the prediction
	Stats cts;
	uint s,t;
	bool predQ,actQ;
	for (s=0;s<pred.size();s++){
		for (t=0;t<pred[s].scl.Seqlen;t++){
			if (atomQ) predQ=pred[s].row[t].predbeta_atomQ;
			else predQ=pred[s].row[t].predbetaQ;
			actQ=pred[s].row[t].actbetaQ;

			if      ( predQ &&  actQ) cts.P++;
			else if ( predQ && !actQ) cts.O++;
			else if (!predQ &&  actQ) cts.U++;
			else if (!predQ && !actQ) cts.N++;
		}
	}
	return cts;
}


float Sov1999(Seq& S,int istate_beta,char state){
	//wrapper function for sov using defaults:
	//Proteins 1999 method
	//compute Sov(all segments, H,E,C)
	uint t,T=S.scl.Seqlen;
	string psec,osec;
	for (t=0;t<T;t++){
		if (Par::ReduxEval[S.row[t].pred_rstate] == (unsigned int)istate_beta) psec+='E';
		else psec+='C';
		if (Par::ReduxEval[S.row[t].act_rstate] == (unsigned int)istate_beta) osec+='E';
		else osec+='C';
	}

	parameters data;

	switch (state){
	case 0:    data.sov_what=0; break;
	case 'H':  data.sov_what=1; break;
	case 'E':  data.sov_what=2; break;
	case 'C':  data.sov_what=3; break;
	default: Error("Sov1999: Error, state must be H, E, or C");
	}

	//using a default constructor for struct parameters.
	return sov(T,&psec[0],&osec[0],&data);
}
