#include "Par.h"
#include "Tools.h"
#include "Serialize.h"
#include "Load.h"
#include <set>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cmath>
#include <iomanip>
#include <cstdio>
#include <algorithm>

using namespace std;
extern int quiet;

                               // i=int,s=string,d=double
map<char,unsigned int> Par::AminoMap;        // Aminomap[sam]=iam;
map<unsigned int,char> Par::AminoMapRev;
map<string,unsigned int> Par::SLNmap, Par::SANmap, Par::SENmap;
map<string,unsigned int> Par::Evalmap, Par::Decodemap;
map<unsigned int,string>Par::SLNrev,Par::SANrev,Par::SENrev;
vector<string> Par::Evalrev, Par::Decoderev;
unsigned int Par::Begin_ian, Par::End_ian;
map<string,string>Par::L2Lmap; //L2Lmap[sln_old]=sln_new
map<unsigned int,set<unsigned int> >Par::Connect;
vector<vector<bool> >Par::LA; //LA[iln][ian]=bool
vector<unsigned int>Par::ReduxDecode,Par::ReduxEval; //ReduxStates[ian]=istate
unsigned int Par::NDecode,Par::NEval; //Number of decode or eval states
vector<int>Par::A2E; //A2E[ian]=ien
unsigned int Par::NumE, Par::NumA, Par::NumATotal, Par::NumL;
unsigned int Par::NUMAMINO;
double Par::Slope,Par::Intercept;
double Par::DelMin, Par::DelMax, Par::APlus, Par::AMinus, Par::Del;
int Par::Algorithm, Par::Objective, Par::PathSelection;
double Par::Regularizer;
bool Par::RegularizeQ;
dirichlet Par::Dirichlet;
zcal Par::ZCalibration;
vector<double>Par::AAComp; //AAComp[c]=double
vector<pair<double,double> >Par::Zcurve;
set<string>Par::SStates;

void Par::Clear(){
	//erases all static data in class Par
	Connect.clear();
	AminoMap.clear();
	AminoMapRev.clear();
	A2E.clear();
 	SLNmap.clear();
 	SANmap.clear();
 	SENmap.clear();
	Evalmap.clear();
	Decodemap.clear();
	SLNrev.clear();
 	SANrev.clear();
 	SENrev.clear();
	Evalrev.clear();
	Decoderev.clear();
	SStates.clear();
 	L2Lmap.clear();
	LA.clear();
	ReduxDecode.clear();
	ReduxEval.clear();
	AAComp.clear();
	Zcurve.clear();
	ZCalibration.z.clear();
	ZCalibration.acc.clear();
	ZCalibration.cov.clear();
	NDecode=NEval=NumE=NumA=NumATotal=NumL=0;
	NUMAMINO=0;
	Slope=0;
	Intercept=0;
}



void Par::Init(char* l2lfile,char* l2afile,char* a2efile,
			   char* confile,
			   char* abet, double del, int algo,
			   int objective, double regularizer){
	//initializes all static variables for Par
	Clear();
	InitConst(0.0,0.0,del, algo, objective, regularizer);
	InitAbet(abet);
	InitMaps(l2lfile,l2afile,a2efile,confile);
}



void Par::InitConst(double s, double i, double del,
					int algo, int obj, double reg){
	Slope=s;
	Intercept=i;
	APlus = 1.2;
	AMinus = 0.5;
	DelMin = pow(10.0, -20.0);
	DelMax = 10.0;
	Del = del;
	Algorithm = algo;
	Objective = obj;
	Regularizer = reg;
}


void Par::Init(const char *abet){
	InitAbet(abet);
}


Par::Par(){}

void Par::LoadStrandStates(char *ssfile){

	ifstream infile(ssfile);
	if (! infile) {
		cerr<<"LoadStrandStates: Couldn't open strandstates file "<<ssfile
			<<", exiting...";
		exit(1);
	}
	string ss;
	while (infile>>ss) Par::SStates.insert(ss);
	infile.close();
}

Par::Par(vector<TrainSeq>&tsq,char* cf,set<string>& ex,bool profQ){
	//must call Init before calling Par, and in this order
	this->Allocate(Par::NumATotal,Par::NumE);
	this->InitArch(1.0);
	this->InitEmit(1.0);
}


void Par::Init(char* l2lfile, const char *abet){
	Clear();
	InitAbet(abet);
	LoadL2L(l2lfile);
}


void Par::InitAbet(const char *abet){
	//initialize AminoMap and NUMAMINO and AAComp
	string aa=abet;
	//this is a requirement for the symbols
	NUMAMINO=aa.length();
	AAComp.resize(NUMAMINO);
	for (unsigned int i=0;i<NUMAMINO;i++){
		AminoMap[aa[i]]=i;
		AminoMapRev[i]=aa[i];
		AAComp[i]=0;
	}
}	


void Par::InitMaps(char* l2lfile,char* l2afile,
				   char* a2efile,char* confile){
	//static wrapper function that calls four functions
	//in correct order
	LoadConnect(confile);
	if( !quiet ) cerr<<"Loaded "<<confile<<endl;
	LoadL2L(l2lfile);
	if( !quiet ) cerr<<"Loaded "<<l2lfile<<endl;
	LoadL2A(l2afile);
	if( !quiet ) cerr<<"Loaded "<<l2afile<<endl;
	LoadA2E(a2efile);
	if( !quiet ) cerr<<"Loaded "<<a2efile<<endl;
}


void Par::InitRedux(char* rdfile,char* refile){

	LoadRedux(rdfile,Decoderev,SANmap,ReduxDecode);
	Decodemap = CreateMap(Decoderev);
	if( !quiet ) cerr<<"Loaded "<<rdfile<<endl;
	LoadRedux(refile,Evalrev,Decodemap,ReduxEval);
	Evalmap = CreateMap(Evalrev);
	if( !quiet ) cerr<<"Loaded "<<refile<<endl;
	NDecode = Decodemap.size();
	NEval = Evalmap.size();
}


void Par::InitDirichlet(char *dfile){
	//initialize dirichlet parameters from storage file
	ifstream infile(dfile);
	if (! infile) {
		cerr<<"InitDirichlet: Couldn't open input file "<<dfile
			<<", exiting...";
		exit(1);
	}

	double D = 0.0;
	ReadVector(infile, Par::Dirichlet.coeff, D);
	Read2DVector(infile, Par::Dirichlet.alpha, D);
	infile.close();

	//check integrity of data
	bool errorQ = false;
	unsigned int Ncomp = Par::Dirichlet.coeff.size();
	if (Par::Dirichlet.coeff.size() != Par::Dirichlet.alpha.size()){
		errorQ = true;
		cerr<<"InitDirichlet: found "<<Par::Dirichlet.coeff.size()
			<<" mixture coefficients but "<<Par::Dirichlet.alpha.size()
			<<" sets of alpha variables."<<endl;
	}
	for (unsigned int j=0; j < Ncomp; j++){
		if (Par::Dirichlet.alpha[j].size() != Par::NUMAMINO){
			errorQ = true;
			cerr<<"InitDirichlet: mixture "<<j<<" has "
				<<Par::Dirichlet.alpha[j].size()<<" alpha variables"
				<<" unequal to alphabet size of "<<Par::NUMAMINO<<endl;
		}
		if (Par::Dirichlet.coeff[j] <= 0.0) {
			errorQ = true;
			cerr<<"InitDirichlet: mixture coefficient "
				<<j<<" is not a positive number: "
				<<Par::Dirichlet.coeff[j];
		}
		for (unsigned int i=0; i < Par::NUMAMINO; i++){
			if (Par::Dirichlet.alpha[j][i] <= 0.0){
			errorQ = true;
			cerr<<"InitDirichlet: alpha variable "
				<<i<<" of mixture component "<<j
				<<" is not a positive number: "
				<<Par::Dirichlet.alpha[j][i];
			}
		}
	}
	if (errorQ) exit(1);
	
}


void Par::InitZCalibration(char *zc) {
	ifstream infile(zc);
	if (! infile) {
		cerr<<"InitDirichlet: Couldn't open input file "<<zc
			<<", exiting...";
		exit(1);
	}
	char line[100];
	float z, cov, acc;
	//float *z = , *cov, *acc;

	map<double, pair<double, double> >zmap;
	map<double, pair<double, double> >::iterator zit;

	while (infile.getline(line,100)){
		sscanf(line, "%f %f %f", &z, &acc, &cov);
		zmap[z] = make_pair(acc,cov);
	}
	infile.close();

	
	for (zit = zmap.begin(); zit != zmap.end(); zit++){
		Par::ZCalibration.z.push_back(zit->first);
		Par::ZCalibration.acc.push_back(zit->second.first);
		Par::ZCalibration.cov.push_back(zit->second.second);
	}
}


void Par::LoadConnect(char* cf) throw (string&){
	//Loads the architecture file, (option -r)
	//The format is a sparse matrix representation
	//of the nodes of the HMM, e.g.

	//san  san san san san
	//san  san san san san san
	//...

	//the first san (Architecture node) is the source
	//the remaining ones on that line are those which it
	//connects to.

	ostringstream errs;

	ifstream cfile;
	cfile.open(cf);
	if (!cfile) {
		errs<<"Fatal Error: Couldn't open Architecture file "
			<<cf<<" (option -r)";
		throw errs.str();
	}
	
	string san, s_san, t_san;
	unsigned int ian, s_ian, t_ian;
	unsigned int ian_ctr=0;

	//Parse once to load SANmap and SANrev
	while (cfile>>san){
		if (SANmap.find(san)==SANmap.end() &&
			san != "BEGIN" &&
			san != "END"){
			SANmap[san] = ian_ctr;
			SANrev[ian_ctr] = san;
			ian_ctr++;
		}
	}

	NumA=ian_ctr;
	NumATotal = NumA + 2;
	SANmap["BEGIN"] = Begin_ian = NumA;
	SANmap["END"]= End_ian = NumA+1;
	SANrev[Begin_ian] = "BEGIN";
	SANrev[End_ian] = "END";

	cfile.clear();
	cfile.seekg(ios::beg);

	while (cfile>>s_san){
		s_ian=SANmap[s_san];

		if (Connect.find(s_ian)!=Connect.end()) {
			errs<<"E1: repeat source "<<s_san.data()<<"found.";
			throw errs.str();
		}

		Connect[s_ian] = set<unsigned int>();
		while (cfile.peek()!='\n') {
			cfile>>t_san;
			t_ian=SANmap[t_san];
			Connect[s_ian].insert(t_ian);
		}
	}
	cfile.close();

	//Check for architectural integrity
	errs.str("");
	if (Connect.find(Begin_ian) == Connect.end() ||
		Connect[Begin_ian].size() == 0)
		errs<<"Architecture has no BEGIN node or it has no targets."<<endl;
	if (Connect.find(End_ian) != Connect.end())
		errs<<"Architecture specifies END node as a source node."<<endl;
	for (ian = 0; ian < NumA; ian++)
		if (Connect.find(ian) == Connect.end() ||
			Connect[ian].size() == 0)
			errs<<"Architecture hasn't specified targets for "<<SANrev[ian]<<"."<<endl;
	bool EndQ = false;
	for (ian = 0; ian < NumA; ian++)
		if (Connect[ian].find(End_ian) != Connect[ian].end())
			EndQ = true;
	if (! EndQ) errs<<"Architecture doesn't specify END node as a target for any node."<<endl;
	if (errs.str().size() != 0) throw errs.str();
}
			

void Par::LoadL2L(char* l2lfile) throw (string&){
	//static function
	//loads SLNmap and SLNrev
	//parses Label2Label file (option -l), e.g.

	//group_label individual_label individual_label individual_label ...
	//group_label individual_label individual_label ...

	//individual_label is a label found in the sequence-file (option -s)
	//group_label is the relabeling, and represents a reduction of
	//information

	//This allows the flexibility of using a sequence-file (option -s)
	//with extremely specific labels, and then merging them

	//all labels must be two digits
	//if a label is encountered in the training sequences
	//which isn't in the l2lmap, it is an error	

	ostringstream errs;
	ifstream lf(l2lfile);
	if (!lf) {
		errs<<"Couldn't open label-to-label file "<<l2lfile;
		throw errs.str();
	}
	string sln_new,sln_old;
	unsigned int iln=0;
	unsigned int cur_iln;
	map<string,string>l2l;
	set<string>seen_sln_old;
	while (lf>>sln_new){
		if (sln_new.size()==0){
			errs<<"E2: found blank line in "<<l2lfile;
			throw errs.str();
		}
		if (SLNmap.find(sln_new)!=SLNmap.end()){
			errs<<"E3: repeat sln_new found in "<<l2lfile<<": "<<sln_new.data();
			throw errs.str();
		}

		SLNmap[sln_new]=iln;
		SLNrev[iln]=sln_new;
		iln++;

		cur_iln=SLNmap[sln_new];

		//unfortunately mis-parses trailing tabs or spaces at end of line.
		while (lf.peek() != '\n' && lf.peek() != EOF){
			lf>>sln_old;
			if (sln_old.size()==0){
				errs<<"E4: new label has no old labels in "<<l2lfile;
				throw errs.str();
			}
			if (seen_sln_old.find(sln_old)!=seen_sln_old.end()){
				errs<<"E5: repeat sln_old found: "
					<<sln_old.data()<<" in "<<l2lfile;
				throw errs.str();
			}
			seen_sln_old.insert(sln_old);
			L2Lmap[sln_old]=sln_new;
		}
	}
	NumL=iln;
	lf.close();
}


void Par::LoadL2A(char* l2afile) throw (string&){
	//this is a static function

	// parses l2afile, format:
	// sln  san san san...
	// sln  san san san...
	// creates LA[iln][ian]=bool

	//The mapping of the set of reduced labels (after reduction by LoadL2L)
	//to the set of architecture nodes (san's) must only be onto
	//(a.k.a. all san's are accounted for by at least one sln)
	//It is perfectly legal to have more than one sln map to a given san

	ostringstream errs;
	ifstream lf(l2afile);
	if (!lf) {
		errs<<"Couldn't open label2arch-map file "<<l2afile
			<<" (option -a).";
		throw errs.str();
	}
	string sln,san;
	unsigned int iln,ian,cur_iln,cur_ian;
	map<unsigned int,set<unsigned int> > l2a;
	set<string> used_san;


	while (lf>>sln){
		if (sln.size()==0){
			errs<<"E2: Found blank line in "<<l2afile;
			throw errs.str();
		}

		if (SLNmap.find(sln)==SLNmap.end()){
			errs<<"E7: Label "<<sln.data()
				<<" found in "<<l2afile<<" but missing from L2Lfile.";
			throw errs.str();
		}

		cur_iln=SLNmap[sln];

		while (lf.peek() != '\n' && lf.good()){
			lf>>san;
			if (san.size()==0){
				errs<<"E8: label has no archnames in"<<l2afile;
				throw errs.str();
			}

			if (SANmap.find(san)==SANmap.end()){
				errs<<"E9: "<<san.data()<<" not found in arch file.";
				throw errs.str();
			}

			if (used_san.find(san)==used_san.end())
				used_san.insert(san);
			//it is okay to have repeat san's in explicit mapping
			
			cur_ian=SANmap[san];
			if (l2a.find(cur_iln)==l2a.end())
				l2a[cur_iln]=set<unsigned int>();
			if (l2a[cur_iln].find(cur_ian)==l2a[cur_iln].end())
				l2a[cur_iln].insert(cur_ian);
		}
	}
	lf.close();

	//Achieve Implicit mapping, with error checks
	// Here we 
	map<string,unsigned int>::iterator it;
	string test_san;
	for (it=SLNmap.begin();it!=SLNmap.end();it++){
		sln=it->first;
		cur_iln=it->second;
		if (l2a.find(cur_iln)!=l2a.end()) continue;
		//ensures only implicit (absent from l2afile) are treated

		test_san=sln; //we will test to see if it exists in SANmap
		if (SANmap.find(test_san)==SANmap.end()){
			errs<<"E10: "<<sln.data()<<" was found in label-to-label file "
				<<"but not in architecture file.";
			throw errs.str();
		}
		cur_ian=SANmap[test_san];

// 		if (used_san.find(test_san)!=used_san.end())
// 			cerr<<"LoadL2A: warning: implicitly mapped sln "<<sln
// 				<<" has a synonymous san which has been used "
// 				<<"in an explicit mapping"<<endl;
		l2a[cur_iln]=set<unsigned int>();
		l2a[cur_iln].insert(cur_ian);
// 		cout<<"Creating implicit mapping between sln "<<SLNrev[cur_iln]
// 			<<" and san "<<SANrev[cur_ian]<<endl;
	}

	//Check that all iln's and all ian's are accounted for
	//in the l2a mapping

	bool ianQ;
	for (ian=0;ian<NumA;ian++){
		if (SANrev[ian] == "BEGIN" ||
			SANrev[ian] == "END") continue;
		ianQ=false;
		for (iln=0;iln<NumL;iln++){
			if (l2a.find(iln)==l2a.end()){
				errs<<"E11: Error loading label2arch-map.  Label "
					<<SLNrev[iln].data()
					<<"exists in training sequences but is not "
					<<"mapped to any architecture node.";
				throw errs.str();
			}
			if (l2a[iln].find(ian)!=l2a[iln].end()) ianQ=true;
		}
		if (!ianQ) {
			errs<<"E12: Error loading label2arch-map.  Architecture node "
				<<SANrev[ian].data()
				<<"exists in arch-file (option -r) but does not exist in "
				<<"label2arch-map (option -a)";
			throw errs.str();
		}
	}

			
	//set LA
	LA.resize(NumL);
	for (iln=0;iln<NumL;iln++){
		LA[iln].resize(NumA);
		for (ian=0;ian<NumA;ian++){
			if (l2a[iln].find(ian)!=l2a[iln].end()){ //we have a connection
				LA[iln][ian]=true;
				/*
				cout<<"sln "<<SLNrev[iln]<<" maps to san "
					<<SANrev[ian]<<endl;
				*/
			}
			else LA[iln][ian]=false;
		}
	}
}
	

void Par::LoadA2E(char* a2efile) throw (string&){	
	// static function, parses A2Efile, producing:
	// A2Efile format:
	// sen  san  san  san  san
	// sen  san  san
	// etc.
	// SENmap[sen]=ien, SENrev[ien]=sen
	// A2E[ian]=ien
	// must be called after LoadL2A
	// requirements: sen's and san's must be unique in
	// the file.  the complete set of san's is already
	// there.  so, if we don't see an san, assume
	// it has no reduction.

	ostringstream errs;
	ifstream af(a2efile);
	if (!af) {
		errs<<"Couldn't open arch-to-emission map "<<a2efile
			<<" (option -e).";
		throw errs.str();
	}
	string san,sen;
	unsigned int sen_ctr=0;
	unsigned int ian,ien;

	A2E.resize(NumA);
	set<string>seen_san;

	while (af>>sen){
		if (sen.size()==0){
			errs<<"E2: found blank line in "<<a2efile;
			throw errs.str();
		}
		if (SENmap.find(sen)==SENmap.end()){
			SENmap[sen]=sen_ctr;
			SENrev[sen_ctr]=sen;
			sen_ctr++;
		}
		ien=SENmap[sen];
		
		while (af.peek() != '\n' && af.peek() != EOF){
			af>>san;
			if (san.size()==0){
				errs<<"E12: emission has no archnames in "<<a2efile;
				throw errs.str();
			}
			if (SANmap.find(san) == SANmap.end()){
				errs<<"E13: san "<<san.data()
					<<" found in arch-to-emission file "<<a2efile
					<<" but not seen in architecture file (option -r).";
				throw errs.str();
			}
			if (SANmap[san] == Par::Begin_ian || SANmap[san] == Par::End_ian){
				errs<<"E14: BEGIN and END states are non-emitting and"
					<<" must be absent from tying file "<<a2efile;
				throw errs.str();
			}

			ian=SANmap[san];
			assert(ian < A2E.size());
			A2E[ian]=ien;
			seen_san.insert(san);
			/*
			cout<<"san "<<SANrev[ian]<<" maps to sen "<<sen<<endl;
			*/
		}
	}
	af.close();

	//load the implied states of SENmap, SENrev, A2E
	map<string,unsigned int>::iterator it;
	for (it=SANmap.begin();it!=SANmap.end();it++){
		san = it->first;
		ian = SANmap[san];
		if (ian >= A2E.size()) continue;

		if (seen_san.find(san)==seen_san.end()){
			sen=san;
			SENmap[sen]=sen_ctr;
			SENrev[sen_ctr]=sen;
			sen_ctr++;
			ien=SENmap[sen];
			A2E[ian]=ien;
		}
	}

	NumE=sen_ctr;
}

/*
initialize string->int map of sources
translate targets using existing string->int map
initialize int->int map of targets->sources
*/

void Par::LoadRedux(char *file,
					vector<string>& srcmap,
					map<string, unsigned int>& tarmap,
					vector<unsigned int>& redux) throw (string&){
	
	//load mapping in file, translating first string on each line
	//('source') into an integer, storing it in srcmap
	//use tarmap to translate target strings, storing the integer
	//mapping in redux.  It is stored as target->source

	ostringstream errs;
	string src, tar; //sstate is state of interest user sees
	unsigned int isrc;

	ifstream fh(file);

	if (!fh) {
		errs<<"Couldn't open "<<file<<".";
		throw errs.str();
	}

	//find maximum itar in tarmap
	map<string, unsigned int>::iterator tit;
	set<string> remain;
	set<string>::iterator rit;
	for (tit = tarmap.begin(); tit != tarmap.end(); tit++){
		if (tit->second == Par::Begin_ian ||
			tit->second == Par::End_ian) continue;
		remain.insert(tit->first);
	}

	while (fh>>tar && ! fh.eof()){
		srcmap.push_back(tar);
		while (! fh.eof() && fh.peek()!='\n'){
			fh>>src;
			if (tarmap.find(src) == tarmap.end()){
				errs<<"E14: "<<src.data()<<" found in redux file "
					<<file<<" but there is no translation for it.";
				throw errs.str();
		   	}
			isrc = tarmap[src];
			if (redux.size() <= isrc) redux.resize(isrc + 1);
			redux[isrc] = srcmap.size()-1;
			remain.erase(src);
		}
	}
	fh.close();

	if (remain.size() > 0) {
		errs<<"In redux file "<<file<<" architecture nodes ";
		for (rit = remain.begin(); rit != remain.end(); rit++)
			errs<<*rit<<", ";
		errs<<" were not accounted for."<<endl;
		throw errs.str();
	}
}



void Par::IncrComp(Seq& S){
	//increments the amino acid composition stored in M
	//with the counts found in S
	for (unsigned int t=0;t<S.scl.Seqlen;t++)
		for (unsigned int c=0;c<Par::NUMAMINO;c++)
			AAComp[c]+=S.Profile[t][c];
}



//double Par::Update(vector<TrainSeq>&tsq, set<string>& exclude){
//	if (Par::Algorithm == BAUM_WELCH) return BaumWelchStep(tsq, exclude);
//	else return GradientStep(tsq,exclude);
//}

void Par::PrintZCurve(vector<pair<double,double> >& integral_stats,char *ofile){
	//prints in tab delimited format, (mean, sd)
	//we assume integral_stats has entries starting at zero
	ofstream os(ofile);
	ostringstream errs;
	if (!os){
		errs<<"Couldn't open Zcurve output file "<<ofile<<" (option -z)";
		throw errs.str();
	}

	for (int ind=0;ind<(int)integral_stats.size();ind++){
		os<<integral_stats[ind].first<<'\t'
		  <<integral_stats[ind].second<<endl;
	}
	if( !quiet )cerr<<"Printed "<<ofile<<endl;
	os.close();
}


void Par::ReadZCurve(char *ifile) throw (string&){
	//assume we are reading a file created by PrintZCurve
	//which contains tab-separated lines (mean,sd)
	ifstream is(ifile);
	ostringstream errs;
	if (!is){
		errs<<"Couldn't open Zcurve file "<<ifile<<" (option -z)";
		throw errs.str();
	}

	char line[100];
	float mean,sd;
	while (is.getline(line,100)){
		if (line[0]=='\0') break;
		sscanf(line,"%f\t%f",&mean,&sd);
		Zcurve.push_back(make_pair(mean,sd));
	}
	is.close();
}


void Par::ReadNullFreq(char *ifile) throw (string&){
	//Load NULL Frequency file

	char aa;
	float comp;
	ifstream is(ifile);
	ostringstream errs;
	if (!is){
		errs<<"Couldn't open Zcurve file "<<ifile<<" (option -z)";
		throw errs.str();
	}

	char line[200];

	while (is.getline(line,200)){
		sscanf(line,"%c\t%f\n",&aa,&comp);
		Par::AAComp[Par::AminoMap[aa]] = comp;
	}
	is.close();
	Normalize(&Par::AAComp[0],Par::NUMAMINO);
}

