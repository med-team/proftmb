#include <vector>
#include <map>
#include <dirent.h>
#include <sys/stat.h> //for stat(), checking whether file or directory
#include <string>
#include <iostream>
#include <sstream>
#include <iterator>
#include <cmath>
#include <iomanip>
#include <cstdio>
#include <string.h>
#include <errno.h>
#include "Par.h"
#include "TrainSeq.h"
#include "Load.h"
#include "Output.h"
#include "HMMOutput.h"
#include "Tools.h"
#include "Zscore.h"
#include "opt.h"
#include "Eval.h"
#include "../config.h"

using namespace std;

char *Dir = const_cast<char *>(""),
	*Seqs=const_cast<char *>(""),
	*QFileorDir=const_cast<char *>(""),
	*TestQList=const_cast<char *>(""),
	*ReduxDecode=const_cast<char *>(""),
	*ReduxReport=const_cast<char *>(""),
	*Arch=const_cast<char *>(""),
	*OutPrefix=const_cast<char *>(""),
	*OutPretty=NULL,
	*OutTab=NULL,
	*OutDat=NULL,
	*NullFreq=const_cast<char *>(""),
	*Alphabet=const_cast<char *>("ACDEFGHIKLMNPQRSTVWY"),
	*EmissionsFile=const_cast<char *>("");
char *Zfile=const_cast<char *>("");
char *AccCovZfile=const_cast<char *>("");
char *SeqName=const_cast<char *>("");
char *StaticParams=const_cast<char *>("");
char *TrainedParams=const_cast<char *>("");
char *StrandStates=const_cast<char *>("");
int opt_version = 0;
int quiet = 0;
double ScoreCutoff;

void PrintGlobals(ostream& out){
	out<<"OutPrefix: "<<OutPrefix<<endl
	   <<"QFileorDir: "<<QFileorDir<<endl
	   <<"TestQList: "<<TestQList<<endl
	   <<"AccCovZfile: "<<AccCovZfile<<endl
	   <<"Zfile: "<<Zfile<<endl
	   <<"ReduxDecode: "<<ReduxDecode<<endl
	   <<"ReduxReport: "<<ReduxReport<<endl
	   <<"StaticParams: "<<StaticParams<<endl
	   <<"TrainedParams: "<<TrainedParams<<endl
	   <<"StrandStates: "<<StrandStates<<endl
	   <<"opt_version: "<<opt_version<<endl
	   <<"quiet: "<<quiet<<endl;
}


void ProcessOneFile(const char *, ofstream&, ofstream&, 
					const char *, double, Par&, const char*, bool) throw (string &);


int Run(int argc,char** argv)
{
	if( opt_version )
	{
		cout << PACKAGE_STRING << endl;
		cout << "Copyright (C) 2004 Henry Bigelow" << endl;
	cout << "" << endl;
	cout << "This program is free software: you can redistribute it and/or modify\n"
"it under the terms of the GNU General Public License as published by\n"
"the Free Software Foundation, either version 3 of the License, or\n"
"(at your option) any later version.\n"
"\n"
"This package is distributed in the hope that it will be useful,\n"
"but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
"GNU General Public License for more details.\n"
"\n"
"You should have received a copy of the GNU General Public License\n"
"along with this program. If not, see <http://www.gnu.org/licenses/>." << endl;
		return 0;
	}

	//uint i;

	Seq::InitSeq();

	char fReduxDecode[256],fReduxReport[256],
		fNullFreq[256],fEmissionsFile[256],fZfile[256],
		fAccCovZfile[200],
		fTrainedParams[200],
		fStaticParams[200],
		fStrandStates[200]
		;

	PrefixDir(Dir,ReduxDecode,fReduxDecode);
	PrefixDir(Dir,ReduxReport,fReduxReport);
	PrefixDir(Dir,NullFreq,fNullFreq);
	PrefixDir(Dir,EmissionsFile,fEmissionsFile);
	PrefixDir(Dir,Zfile,fZfile);
	PrefixDir(Dir,AccCovZfile,fAccCovZfile);
	PrefixDir(Dir,TrainedParams,fTrainedParams);
	PrefixDir(Dir,StaticParams,fStaticParams);
	PrefixDir(Dir,StrandStates,fStrandStates);

	struct stat Qstat;

	if (stat(Dir,&Qstat) ||
		((Qstat.st_mode & S_IFMT) != S_IFDIR)) {
		cerr<<"Couldn't find "<<Dir<<" (option -d) or it wasn't a directory."<<endl;
		return 1;
	}

	if (TestQList[0] != 0 && (stat(TestQList,&Qstat) ||
		((Qstat.st_mode & S_IFMT) != S_IFREG))) {
		cerr<<"Couldn't open Qlist file "<<TestQList<<" (option -v)."<<endl;
		return 1;
	}
	

	char line[256];
	ifstream fQList;
	set<string>QList;
	set<string>SStates; //strand states
	fQList.open(TestQList);
	while (fQList>>line) QList.insert(string(line));
	fQList.close();

	Par Model;

	try {
		Par::ReadStatic(fStaticParams);
		Par::InitRedux(fReduxDecode, fReduxReport);
		Model.Read(fTrainedParams);
		Par::InitZCalibration(fAccCovZfile);
		Par::ReadZCurve(fZfile);
		Par::ReadNullFreq(fNullFreq);
		Par::LoadStrandStates(fStrandStates);
	}

	catch (string& errmsg){
		cerr<<errmsg<<endl;
		return 1;
	}



	if( !quiet ) cerr<<"Finished Initializing model.\n";

	//Initialize Outstream

	ostringstream FileName;
	if( OutPretty ) {
		if( *OutPretty ) FileName << OutPretty; else FileName << "/dev/null";
	} else FileName<<OutPrefix<<"_proftmb_pretty.txt";
	ofstream TS(FileName.str().c_str());
	if (!TS) { cerr<<"Couldn't open Output file "<<FileName<<": "<< strerror(errno) <<endl; return 1; }

	FileName.str("");
	if( OutTab ) {
		if( *OutTab ) FileName << OutTab; else FileName << "/dev/null";
	} else FileName<<OutPrefix<<"_proftmb_tabular.txt";
	ofstream TStab(FileName.str().c_str());
	if (!TStab) { cerr<<"Couldn't open Output file "<<FileName<<": "<< strerror(errno) <<endl; return 1; }

	FileName.str("");
	char Datfile[1000] = "/dev/null"; Datfile[9] = 0; Datfile[999] = 0;
	if( OutDat ) {
		if( *OutDat ) strncpy( Datfile, OutDat, 999 );
	} else {
		FileName<<OutPrefix<<"_dat.txt";
		strncpy(Datfile,FileName.str().c_str(), 999);
	}

	//lkajan: Always create this file to allow make(1) to see it, as an indicator of this prog having run.
	{
		ofstream tsdat(Datfile);
		if (! tsdat){
			cerr<<"Couldn't open datafile "<<Datfile<<": " << strerror(errno) << endl;
			exit(1);
		}
		tsdat<<"Position\tAmino\tPosteriorState\tReducedViterbiState\tU\tD\tO\ti"
			 <<endl<<endl;
		tsdat.close();
	}
		
	//Read in each Test Sequence, evaluate and print out results
	char Path[300]; Path[299] = 0; //for constructing the path

	ostringstream sspath;  //can't use unistd.h chdir, not windows compatible...
	string spath;

	if (stat(QFileorDir,&Qstat) || 
		(((Qstat.st_mode & S_IFMT) != S_IFREG) &&
		((Qstat.st_mode & S_IFMT) != S_IFDIR))) {
		cerr<<QFileorDir<<" (option -q) is not a regular file or a directory."<<endl;
		return 1;
	}

	if ((Qstat.st_mode & S_IFMT) == S_IFDIR){
		struct stat Dstat;
		DIR *Qdir;
		dirent *Qdirent;
		Qdir=opendir(QFileorDir);

		TStab<<RdbHeader(true);

		while ((Qdirent=readdir(Qdir))){
			if (TestQList[0] != '\0' &&
				QList.find(string(Qdirent->d_name))==QList.end()) continue;

			sspath.str("");
			sspath<<QFileorDir<<'/'<<Qdirent->d_name;
			strncpy(Path,sspath.str().c_str(), 299 );

			if (stat(Path,&Dstat))
				cout<<"Can't determine what "<<Path
					<<" is...skipping."<<endl;

			else if ((Dstat.st_mode & S_IFMT)==S_IFREG){

				try {
					ProcessOneFile(Path, TS, TStab, Datfile, 
								   ScoreCutoff, Model, Qdirent->d_name, true);
				}
				catch (string& errmsg){
					cerr<<errmsg<<endl;
					return 1;
				}


			}
			else if ((Dstat.st_mode & S_IFMT)==S_IFDIR)
				cout<<"Skipping subdirectory "<<Qdirent->d_name<<"."<<endl;
			else cout<<"Skipping other type "<<Qdirent->d_name<<"."<<endl;
		}
	}
	else if ((Qstat.st_mode & S_IFMT) == S_IFREG){

		try {
			ProcessOneFile(QFileorDir, TS, TStab, Datfile, 
						   ScoreCutoff, Model, SeqName, false);
		}
		catch (string& errmsg){
			cerr<<errmsg<<endl;
			return 1;
		}

	}
	else { 
		cerr<<QFileorDir<<
			", (option -q) is neither a directory nor a regular file."<<endl;
		return 1;
	}

	if( !quiet ) cerr<<"Successfully finished Prediction.\n";
	return 0;
}




void ProcessOneFile(const char *qfile, ofstream& ts, ofstream& tstab, 
					const char *datfile, double cut, Par& m, const char *name, bool multiQ) throw (string &) {

	ostringstream errs;
	Seq seq;
	try {
		seq = Seq(qfile, name);
	}

	catch (string & errmsg){
		cerr<<"During ProcessOneFile, Seq constructor threw:"
			<<errmsg<<endl;
		exit(1);
	}
	catch (...){
		errs<<qfile<<" is not in psiblast format. "
		   <<"Please check the format and try again."<<endl;
		throw errs.str();
	}

	seq.Posterior(m);

	double zscore = CalcZScore(Par::Zcurve,seq.scl.Seqlen,seq.scl.Pbits);
	seq.scl.Score=zscore;
	struct stat qstat;
	
	if (zscore > cut){
		ofstream tsdat(datfile,ios::app);
		if (! tsdat){
			cerr<<"Couldn't open datafile "<<datfile<<": "<< strerror(errno) << endl;
			exit(1);
		}
		PrintPosteriorDat(tsdat, seq, m, cut);
		tsdat.close();
	}
	
	PrintPretty(ts,seq,cut, 15);
	PrintRdb(tstab,seq,(zscore > cut), multiQ);
	
}



int main(int argc,char** argv){
	OptRegister(&Dir,OPT_STRING,'d',const_cast<char *>("directory-root"),const_cast<char *>("root path where files (options -s,-r,-l,-a,-e,-t,-u,-z,-n) reside"));
	OptRegister(&ReduxDecode,OPT_STRING,'a',const_cast<char *>("reduction-state-decode"),const_cast<char *>("state reduction for decoding"));
	OptRegister(&ReduxReport,OPT_STRING,'b',const_cast<char *>("reduction-state-report"),const_cast<char *>("state reduction for reporting"));
	OptRegister(&StrandStates,OPT_STRING,'m',const_cast<char *>("membrane-strand-states"),const_cast<char *>("list of membrane strand states"));
	OptRegister(&Zfile,OPT_STRING,'z',const_cast<char *>("z-curve-file"),const_cast<char *>("file containing means and sd's at integral length values"));
	OptRegister(&AccCovZfile, OPT_STRING, 'x', const_cast<char *>("z-calibration-curve"),const_cast<char *>("file mapping coverage and accuracy values to z-scores"));
	OptRegister(&NullFreq,OPT_STRING,'n',const_cast<char *>("null-frequency"),const_cast<char *>("background frequency file"));
	OptRegister(&ScoreCutoff,OPT_DOUBLE,'c',const_cast<char *>("minimum-score-cutoff"),const_cast<char *>("minimum z-score for per-residue prediction"));
	OptRegister(&OutPrefix,OPT_STRING,'o',const_cast<char *>("outfile-prefix"),const_cast<char *>("output file prefix for the three files generated: PREFIX_dat.txt, PREFIX_proftmb_pretty.txt PREFIX_proftmb_tabular.txt"));
	OptRegister(&StaticParams, OPT_STRING,'s',const_cast<char *>("static-model-data"),const_cast<char *>("data representing the model architecture"));
	OptRegister(&TrainedParams, OPT_STRING, 't', const_cast<char *>("trained-params"),const_cast<char *>("params representing the encoded training data"));
	OptRegister(&QFileorDir,OPT_STRING,'q',const_cast<char *>("test-blastQ-file-or-dir"),const_cast<char *>("psiblast profile (-Q) or directory (full pathname or relative to current directory) with many profiles"));
	OptRegister(&SeqName,OPT_STRING,'w',const_cast<char *>("single-sequence-name"),const_cast<char *>("if -q option points to a single file, this is the name"));
	OptRegister(&TestQList,OPT_STRING,'v',const_cast<char *>("list-blastQ-files"),const_cast<char *>("list of psiblast files to process in directory (leave blank to process all files)"));
	OptRegister(&OutPretty,OPT_STRING,const_cast<char *>("outfile-pretty"),const_cast<char *>("pretty output file (overrides automatic name PREFIX_proftmb_pretty.txt)"));
	OptRegister(&OutTab,OPT_STRING,const_cast<char *>("outfile-tab"),const_cast<char *>("tabulated output file (overrides automatic name PREFIX_proftmb_tabular.txt)"));
	OptRegister(&OutDat,OPT_STRING,const_cast<char *>("outfile-dat"),const_cast<char *>("data output file (overrides automatic name PREFIX_dat.txt)"));
	OptRegister(&opt_version,OPT_BOOL,const_cast<char *>("version"),const_cast<char *>("output version information and exit"));
	OptRegister(&quiet,OPT_BOOL,const_cast<char *>("quiet"),const_cast<char *>("be quiet"));

	optMain(Run);

	if (argc==1){
		char **myargv,*mybuf[2];
		mybuf[0]=argv[0];
		mybuf[1]=const_cast<char *>("$");
		myargv=mybuf;
		int myargc=2;
		cout<<endl<<endl
			<<"Welcome to ProfTMB.  type '?' at the prompt for "
			<<"instructions on entering options."<<endl
			<<"----------------------------"<<endl<<endl;
		opt(&myargc,&myargv);
	}

	else opt(&argc,&argv);
	return Run(argc,argv);
}
// vim:ai:
