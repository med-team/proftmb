#ifndef _HTools
#define _HTools

#include "Seq.h"
#include "Par.h"

bool TrelQ (unsigned int, unsigned int, Seq&, bool);
bool TrelQ (unsigned int, const set<unsigned int>&);
void PrintFSum(Seq&, Par&, const unsigned int, const unsigned int, bool);
bool NotifyDeadEnd(Seq&, unsigned int, set<unsigned int>&);
bool NotifyDeadEndTerm(Seq&, set<unsigned int>&);
bool NotifyDeadEnd(Seq&, unsigned int, set<unsigned int>&, set<unsigned int>&);

#endif
