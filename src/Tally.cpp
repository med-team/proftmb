#include "TrainSeq.h"
#include "Tools.h"
#include "Par.h"
#include "HTools.h"
#include <set>


void Par::Allocate(unsigned int numA,unsigned int numE) {
	unsigned int ien;
	
	//allocate A,C,Arch
	A_clamp.resize(numA);
	A_free.resize(numA);
	C_clamp.resize(numE);
	C_free.resize(numE);

	for (ien=0;	ien<numE; ien++) {
		C_clamp[ien].resize(NUMAMINO);
		C_free[ien].resize(NUMAMINO);
	}

	zEmit.resize(numE);
	delEmit.resize(numE);
	for (ien = 0; ien < numE; ien++){
		zEmit[ien].resize(Par::NUMAMINO);
		delEmit[ien].resize(Par::NUMAMINO);
	}

}


void Par::InitArch(double prior){
	//initialize Arch, ArchRev, ArchSize
	//data structures according to Par::Connect

	//initialize Arch with normalized values
	map<unsigned int,set<unsigned int> >::iterator mit;
	set<unsigned int>::iterator sit;
	float norm;
	unsigned int NAT = Par::NumATotal;

	Arch.resize(NAT);
	for (mit = Par::Connect.begin(); mit != Par::Connect.end(); mit++){
		norm = 1.0 / (float)mit->second.size();
		for (sit = mit->second.begin(); sit != mit->second.end(); sit++){
			Arch[mit->first].push_back(archpair(*sit,norm));
		}
	}

	//initialize ArchRev
	unsigned int n, ian, s_ian, t_ian;
	ArchRev.resize(NAT);
	for (s_ian=0; s_ian<Arch.size(); s_ian++) ArchRev[s_ian].resize(0);

	for (s_ian=0; s_ian<Arch.size(); s_ian++){
		for (n=0; n < Arch[s_ian].size(); n++){
			t_ian = Arch[s_ian][n].node;
			ArchRev[t_ian].push_back(revpair(s_ian,n));
			//cout<<"ArchRev["<<t_ian<<"]<-"<<s_ian<<", "<<n<<endl;
		}
	}

	//initialize ArchSize
	ArchSize.resize(NAT);
	for (n=0; n<NAT; n++){
		ArchSize[n].nsrc=ArchRev[n].size();
		ArchSize[n].ntar=Arch[n].size();
	}

	//initialize zArch and delArch
	zArch.resize(NAT);
	delArch.resize(NAT);
	for (ian = 0; ian < NAT; ian++){
		zArch[ian].resize(ArchSize[ian].ntar);
		delArch[ian].resize(ArchSize[ian].ntar);
	}

	for (ian=0;	ian<NAT; ian++) {
		A_clamp[ian].resize(ArchSize[ian].ntar);
		A_free[ian].resize(ArchSize[ian].ntar);
	}
}


void Par::InitEmit(double prior){
	//Initializes EmitAmino

	unsigned int i,c;
	EmitAmino.resize(Par::NumE);
	for (i=0; i<Par::NumE; i++){
		EmitAmino[i].resize(Par::NUMAMINO);
		for (c=0; c<Par::NUMAMINO; c++)
			EmitAmino[i][c] = 1.0 / (float)Par::NUMAMINO;
	}
}


void Par::TallyA(vector<TrainSeq>& tsq, set<string>& ex, bool validQ){
	//tallies ML counts for architecture.
	//uses LA to find all SAN's from SLN's
	if (! validQ) return; //no point in training if we ignore
	//labels on training sequences.


	vector<vector<double> >atally(NumATotal); //atally[src_ian][tar_ian]=ct
	unsigned int i,j,s,t,T,ian,s_ian,t_ian,src_iln,tar_iln;

	//zero atally, beg_tally and end_tally
	for (i=0;i<NumATotal;i++) {
		atally[i].resize(NumATotal); //square matrix
		for (j=0;j<NumATotal;j++) atally[i][j]=0.0; //really used as int
	}
	
	//count up all transitions between a src_iln and tar_iln
	//this function makes use of training sequence labels
	for (s=0;s<tsq.size();s++){
		if (ex.find(tsq[s].scl.SeqID) != ex.end()) continue; //exclude sequence
		T=tsq[s].scl.Seqlen;
		
		//count initial transition from begin state:
		for (ian=0;ian<NumA;ian++)
			if (TrelQ(0,ian,tsq[s],validQ)) 
				atally[Par::Begin_ian][ian]++;
		
		//continue
		for (t=1;t<T;t++){
			src_iln=tsq[s].Label[t-1];
			tar_iln=tsq[s].Label[t];
			UpdateA(atally,src_iln,tar_iln);
   			//UpdateA2(atally,src_iln,tar_iln);

		}
		
		//count transition to end state:
		for (ian=0;ian<NumA;ian++)
			if (TrelQ(T-1,ian,tsq[s],validQ)) 
				atally[ian][Par::End_ian]++;
	}
	
	//normalize atally,beg_tally,end_tally
	for (ian=0;ian<NumATotal;ian++){
		BoolNormalize(&atally[ian][0],atally[ian].size());
		// 			cerr<<"TallyA: node "<<ian<<"("<<SANrev[ian]
		// 				<<") has no targets"<<endl;
	}
	//since CA only appears at the very end

	//assign to Arch,Pi,Ep
	for (s_ian=0;s_ian<NumATotal;s_ian++){
		for (t_ian=0; t_ian<NumATotal; t_ian++)
			Arch[s_ian][t_ian].score += atally[s_ian][t_ian];
		//cout<<"atally["<<SANrev[s_ian]<<"]["<<
		//SANrev[t_ian]<<"]="<<atally[s_ian][t_ian]<<endl;
	}
}


void Par::UpdateA(vector<vector<double> >& atally,
				  unsigned int s_iln,unsigned int t_iln){
	//updates the ML count of transitions for
	//all src_ian's and tar_ian's corresponding
	//to s_iln and t_iln which are consistent
	//with the pre-specified architecture as stored
	//in Connect[src_ian] (map<string,set<string> >)
	
	unsigned int s_ian,t_ian;
	for (s_ian=0;s_ian<NumATotal;s_ian++) {
		if (!LA[s_iln][s_ian]) continue; //a hack for finding all s_iln's mapped to s_ian's
		for (t_ian=0;t_ian<NumATotal;t_ian++){
			if (!LA[t_iln][t_ian]) continue; //same hack
			if (Connect[s_ian].find(t_ian)==
				Connect[s_ian].end()) {
				continue;
			}
			atally[s_ian][t_ian]++;
		}
	}	
}


void Par::UpdateA2(vector<vector<double> >& atally,
				  unsigned int s_iln,unsigned int t_iln){
	//updates the ML count of transitions for
	//all s_ian's and t_ian's corresponding
	//to s_iln and t_iln using the 'forward connecting'
	//rule, namely that t_ian must be greater than
	//s_ian
	
	unsigned int s_ian,t_ian;
	for (s_ian=0;s_ian<NumATotal;s_ian++) {
		if (!LA[s_iln][s_ian]) continue; //a hack for finding all s_iln's mapped to s_ian's
		for (t_ian=0;t_ian<NumATotal;t_ian++){
			if (!LA[t_iln][t_ian]) continue; //same hack
			if (s_iln==t_iln && s_ian>t_ian) continue;
			//forward or self connections of
			//iso-labeled arch nodes
			atally[s_ian][t_ian]++;
		}
	}	
}


void Par::TallyE(vector<TrainSeq>& tsq,set<string>& ex, bool validQ){
	//there is really no need for this function at all...
	//perhaps to bootstrap the Baum-Welch cycle?

	//need a way to check if ien was seen or not.

	set<int>seen_ien;
	vector<vector<double> >etally(NumE); //etally[ien][c]=ct
	unsigned int s,t,c,ian,ien,D=tsq.size();

	//allocate and initialize etally and ptally to zero
	for (ien=0;ien<NumE;ien++) {
		etally[ien].resize(NUMAMINO); //NumE x NUMAMINO matrix
		for (c=0;c<NUMAMINO;c++) etally[ien][c]=0.0; //really used as int
	}
	
	//tally profile for each ien,c and prof counts
	for (s=0;s<D;s++){
		if (ex.find(tsq[s].scl.SeqID) != ex.end()) continue; //exclude sequence
		for (t=0;t<tsq[s].scl.Seqlen;t++){
			seen_ien.clear();
			for (ian=0;ian<NumA;ian++){
				if (! TrelQ(t,ian,tsq[s],validQ)) continue;
				ien=A2E[ian];
				
				//here we have a convoluted
				//way to check which iens correspond
				//to the iln labelling
				if (seen_ien.find(ien)==seen_ien.end()){
					seen_ien.insert(ien);
					for (c=0;c<NUMAMINO;c++) //train on profile?
						etally[ien][c]+=tsq[s].Profile[t][c];
				}
			}
		}
	}

	for (ien=0;ien<NumE;ien++){
		
		//etally[ien] can be the zero vector in the case when
		//the sequence excluded from the tally is solely
		//responsible for the ien.  this is very unlikely
		//if NumE is small (few emission states)
		
		//normalize and load into EmitAmino[ien][c]
		BoolNormalize(&etally[ien][0],etally[ien].size());
		//cerr<<"TallyE: no counts for "<<SENrev[ien]<<endl;

		for (c=0;c<NUMAMINO;c++)
			this->EmitAmino[ien][c] += etally[ien][c];
		
	}
}
