#include "Par.h"
#include "Seq.h"
#include <iterator>
#include <algorithm>

using namespace std;


/*******************************************************************
Implementation of Krogh's 1-Best algorithm 

Two methods for improving performance of an HMM and their
application to gene finding.  ISMB5 pp. 179-186

For a partition of architecture nodes into groups corresponding
to the same label, this algorithm is a heuristic to approximate the
most probable labeling.

The induction works as follows:

h_i is the 'best' hypothesis ending at position i
h_ik is the hypothesis for the sum-over-paths ending at (i,k)

Initialization:

for all states l {
  gamma_l(h_1) = a_begin,l * b_1(x_1);
}


Propagation:

for i = 2 to T {
  for all states l{
    for all previous states k{
      c = class corresponding to state k
      d = class corresponding to state l
      gamma_l(h_icd) = [ Sum_k{a_kl*gamma_k(h_ic)} ] * b_l(x_(i+1))
    }
    select and store max over c [gamma_l(h_icd)] as h_(i+1)d
  }
}

*******************************************************************/

void Seq::Krogh1Best(Par& M){

	unsigned int t, k, l, ck, cl, iln;
	unsigned int T = scl.Seqlen, ND = Par::NDecode;
	
	vector<vector<mat> > trel; //a real trellis
	vector<vector<vec> > norm(ND); //a collection of normalization vectors
	vector<vector<mat> > trelcol(ND); // a collection of temporary columns
	vector<vec> normcol(ND);
	
	//Initialization
	vector<set<unsigned int> > hypo(ND);
	for (k = 0; k < Par::NumA; k++){
		ck = Par::ReduxDecode[k];
		for (iln = 0; iln < Par::NumL; iln++)
			if (Par::LA[iln][k]) hypo[ck].insert(k);
	}
	
	vector<vector<unsigned int> > traceback(T);
	for (t = 0; t < T; t++) traceback[t].resize(ND);
	
	
	for (ck = 0; ck < ND; ck++) trelcol[ck].resize(Par::NumA);
	
	//Initialization
	ForwardAlloc(trel, norm[0]); //hack
	ForwardClear(trel, norm[0]);

	for (ck = 0; ck < ND; ck++) {
		norm[ck].resize(T);
		for (t = 0; t < T; t++) norm[ck][t].init_vec();
	}
	
	for (ck = 0; ck < ND; ck++)
		ForwardInit(M, trel[0], norm[ck][0], hypo[ck]);
	
	
	for (l = 0; l < Par::NumA; l++){
		if (trel[0][l].a_aux == 0.0) trel[0][l].a_log = -HUGE_VAL;
		else trel[0][l].a_log = log(trel[0][l].a_aux);
	}

	//Induction
	vector<double>::iterator git;
	
	vector<double> hypo_max(ND);
	//vector<vector<double> > hypo_log(ND);
	//for (ck = 0; ck < ND; ck++) hypo_log[ck].resize(ND);


	vector<unsigned int>ckmax(ND);

	for (t = 1; t < T; t++){


		for (l = 0; l < Par::NumA; l++){
			cl = Par::ReduxDecode[l];

			for (ck = 0; ck < ND; ck++){

				trelcol[ck][l].a_aux = AAux(M, l, t, trel[t-1], hypo[ck], hypo[cl]);
				//hypo_aux[cl][ck] += trelcol[ck][l].a_aux;

				if (trelcol[ck][l].a_aux == 0.0) trelcol[ck][l].a_log = -HUGE_VAL;
				else trelcol[ck][l].a_log =
						 log(trelcol[ck][l].a_aux) + norm[ck][t-1].C_log;
			}

		}

		//find the top-scoring hypothesis for each two-digit hypothesis end
		fill(hypo_max.begin(), hypo_max.end(), -HUGE_VAL);
		for (ck = 0; ck < ND; ck++) for (l = 0; l < Par::NumA; l++){
			cl = Par::ReduxDecode[l];
			if (trelcol[ck][l].a_log == hypo_max[cl] &&
				trelcol[ck][l].a_log != -HUGE_VAL){
				//cout<<"ck="<<ck<<", l="<<l<<", a_log="<<trelcol[ck][l].a_log<<endl;
			}

			if (trelcol[ck][l].a_log > hypo_max[cl]) {
				hypo_max[cl] = trelcol[ck][l].a_log;
				ckmax[cl] = ck;
			}
			
		}

		//Scale trellis columns according to chosen hypotheses
		for (cl = 0; cl < ND; cl++)
			AScale(M, trelcol[ckmax[cl]], norm[ckmax[cl]][t-1].C_log, norm[cl][t], hypo[cl]);


		//Assign individual trellis states to trel
		for (l = 0; l < Par::NumA; l++){
			cl = Par::ReduxDecode[l];
			trel[t][l] = trelcol[ckmax[cl]][l];
		}


		//Assign traceback
		for (cl = 0; cl < ND; cl++) traceback[t][cl] = ckmax[cl];
	}

	//Termination
	vector<double> term(ND);
	for (cl = 0; cl < ND; cl++){
		term[cl] = Term(M, trel[T-1], norm[cl][T-1], hypo[cl]);
	}

	//Traceback
	git = max_element(term.begin(), term.end());

	unsigned int curstate = distance(term.begin(), git);
	
	for (int it = T-1; it >= 0; it--){
		row[it].pred_rstate = curstate;
		curstate = traceback[it][curstate];
	}

	//for (t = T-3; t < T; t++){
	//	for (l = 0; l < Par::NumA; l++) cout<<trel[t][l].a_log<<" ";
	//	cout<<endl;
	//}

	if (scl.labelQ) CalcCTable(M);
}
