#include "Par.h"
#include "HTools.h"
#include <cmath>
#include <iostream>

using namespace std;

//see Bioinformatics: The Machine Learning Approach
//appendix D, online at http://library.books24x7.com
//variable names a=alpha,b=beta,c,C,D are used as in 
//this reference


void Seq::Backward(Par& M){

	unsigned int ian, T = scl.Seqlen;

	set<unsigned int> allnodes;
	for (ian = 0; ian < Par::NumA; ian++) allnodes.insert(ian);

	vector<set<unsigned int> >nodes(T);
	fill(nodes.begin(), nodes.end(), allnodes);

	Backward(M, nodes);
}


void Seq::Backward(Par& M, vector<set<unsigned int> >& nodes){

	unsigned int ian, T = scl.Seqlen, NA = Par::NumA;
	int t;

	BackwardInit(M, trel[T-1], row[T-1], nodes[T-1]);

	for (t = T-2; t >= 0; t--){
		for (ian = 0; ian < NA; ian++)
			trel[t][ian].b_aux = BAux(M, ian, t, trel[t+1], nodes[t], nodes[t+1]);
		BScale(trel[t], row[t], nodes[t]);
	}
}


void Seq::BackwardInit(Par& M,
					   vector<mat>& trellis,
					   vec& row,
					   set<unsigned int>& nodes){

	unsigned int ian, n;
	double aij;

	//a) initialize b_aux(T-1,ian) for all ian not begin or end
	vector<pair<unsigned int, double> >sources =
		M.FindSources(Par::End_ian, nodes);

	for (n=0; n < sources.size(); n++){
		ian = sources[n].first;
		aij = sources[n].second;

		trellis[ian].b_aux = aij;
	}

	BScale(trellis, row, nodes);
}


double Seq::BAux(Par& M,
				 const unsigned int s_ian,
				 const unsigned int t,
				 const vector<mat>& trel2,
				 const set<unsigned int>& nodes1,
				 const set<unsigned int>& nodes2){
	//t refers to first column of which s_ian belongs
	
	assert(t+1 < scl.Seqlen);
	assert (s_ian != Par::Begin_ian && s_ian != Par::End_ian);
	
	if (! TrelQ(s_ian,nodes1)) return 0.0;
	
	unsigned int t_ian, n;
	double aij, aux = 0.0;
	
	vector<pair<unsigned int, double> >targets = M.FindTargets(s_ian, nodes2);
	
	for (n = 0; n < targets.size(); n++){
		t_ian = targets[n].first;
		aij = targets[n].second;
		aux += trel2[t_ian].b_scl * aij * (this->*this->pReturn)(t+1,t_ian,M);
	}
		
	return aux;
}


void Seq::BScale(vector<mat>& trel,
				 vec& row,
				 const set<unsigned int>& nodes){

	unsigned int ian, NA = Par::NumA;

	for (ian = 0; ian < NA; ian++)
		if (TrelQ(ian, nodes)) 
			trel[ian].b_scl = ((row.c == 0.0) ? 0.0 : trel[ian].b_aux / row.c);
}



vector<pair<unsigned int, double> > Par::FindSources(unsigned int t_ian,
													 const set<unsigned int>& valid){
	//find all source ian's and transition probabilities for t_ian

	unsigned int n, fn, s_ian;
	vector<pair<unsigned int, double> >sources;
	double score;

	for (n = 0; n < ArchSize[t_ian].nsrc; n++){

		s_ian = ArchRev[t_ian][n].src;
		fn = ArchRev[t_ian][n].index;
		score = Arch[s_ian][fn].score;

		if (valid.find(s_ian) != valid.end() &&
			s_ian != Par::Begin_ian &&
			s_ian != Par::End_ian)
			sources.push_back(make_pair(s_ian,score));
	}
	
	return sources;
}


vector<pair<unsigned int, double> > Par::FindTargets(unsigned int s_ian,
													 const set<unsigned int>& valid){
	//find all target ian's and transition probabilities for s_ian

	vector<pair<unsigned int, double> > targets;
	double score;
	unsigned int t_ian;

	for (unsigned int n = 0; n < ArchSize[s_ian].ntar; n++){
		t_ian = Arch[s_ian][n].node;
		score = Arch[s_ian][n].score;
		
		//cout<<"First elem: "<<*valid.begin()<<", t_ian="<<t_ian<<endl;

		if (valid.find(t_ian) != valid.end()){

			if (t_ian != Par::Begin_ian &&
				t_ian != Par::End_ian){
				
				targets.push_back(make_pair(t_ian,score));
			}
		}
	}
	//cout<<"targets.size()="<<targets.size()<<endl;
	return targets;
}
