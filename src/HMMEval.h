#ifndef _HMMEval
#define _HMMEVal

#include <vector>
#include "TrainSeq.h"
#include "Seq.h"

Stats EvalPred(std::vector<Seq>&,bool=false);
Stats EvalPred(std::vector<TrainSeq>&,bool=false);
float Sov1999(Seq&,int,char=0);

#endif
