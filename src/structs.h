#ifndef _Structs
#define _Structs

#include <string>
#include <vector>
#include <iostream>
#include <cmath>

using std::string;
using std::vector;
using std::ostream;

typedef unsigned int uint;

struct names {
	unsigned int iln,ian,ien;
	string sln,san;
	char cln[2];
	bool betaQ;
	friend ostream& operator<<(ostream&,names&);
	names(){}
	names(unsigned int l,unsigned int a,unsigned int e,string sln,string san,bool b):
		iln(l),ian(a),ien(e),sln(sln),san(san),betaQ(b){
		cln[0]=sln[0];
		cln[1]=sln[1];
	}
};


struct tarsrcpair { //used for ArchSize
	unsigned int ntar,nsrc;
	tarsrcpair(unsigned int t = 0, unsigned int s = 0):ntar(t),nsrc(s){}
};

struct archpair { //used for Arch
	unsigned int node;
	double score;
	archpair(unsigned int n = 0, double s = 0.0):node(n),score(s){}
};

struct revpair { //used for ArchRev
	unsigned int src,index;
	revpair(unsigned int s = 0, unsigned int i = 0):src(s),index(i){}
};


class evaldat {
 public:
	string cl,id,cd; //class, id, classdesc
	int cdct; //cd counts (number of proteins in this class
	bool posQ;
	unsigned int length;
	double pbits,zscore,evalscore; //evalscore used by scocov and acccov
	friend ostream& operator<<(ostream&,evaldat&);
	evaldat(string& c,string& i,string& cd,bool p,unsigned int l,
			double pb,double z,double e):
		cl(c),id(i),cd(cd),posQ(p),length(l),
		pbits(pb),zscore(z),evalscore(e){}
};


class triplet {
	string cl;
	double sco,cov;	
 public:
	friend ostream& operator<<(ostream&,triplet&);
	triplet(string,double,double);
};


struct scpair {
	double sco,cov;
	scpair(double s,double c):sco(s),cov(c){}
};


struct roc {
	double tpf,fpf,tpf_sd;
	double sco;
	roc(double tpf=0.0,double fpf=0.0,double tpf_sd=0.0,
		double sco=0) :
		tpf(tpf),fpf(fpf),tpf_sd(tpf_sd),sco(sco){}
};


struct Stats{
	double P,O,U,N;
	Stats():P(0.0),O(0.0),U(0.0),N(0.0){}
};


struct mat {
	double a, a_scl, a_aux, a_log, b, b_scl, b_aux, b_log, cs, cs_log, as;
	double a_aux_b,a_aux_n;
	uint s_atom;
	//cumulative-score(for viterbi decoding)
	//alpha,scaled alpha,auxiliary alpha
	//beta, scaled beta,auxiliary beta
	//atomic score,source-atom
	//source set (which set (beta/non-beta) is the source set)
	mat(): a(0),a_scl(0), a_aux(0), a_log(-HUGE_VAL),
		 b(0), b_scl(0),  b_aux(0), b_log(-HUGE_VAL),
		 cs(0), cs_log(-HUGE_VAL), as(0),
		 a_aux_b(0), a_aux_n(0), s_atom(0){}
};


struct measure{

	bool printSD;
	double meanQ, sdQ;
	double meanSOVPos, sdSOVPos;
	double meanSOVALL, sdSOVALL;
	std::vector<double> meanQpred, sdQpred;
	std::vector<double> meanQobs, sdQobs;
	std::vector<double> meanMCC, sdMCC;
	char pfx;

	measure();
	public:
	static string PrintHeader(std::vector<measure>&);
	static string PrintData(std::vector<measure>&);
	friend ostream& operator<<(ostream&, measure&);
};

#endif
