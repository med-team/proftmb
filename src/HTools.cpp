#include "HTools.h"
#include "Par.h"
#include <iostream>
#include <iomanip>

using namespace std;


bool TrelQ(unsigned int t,unsigned int ian,Seq& S,bool validQ){
 	//tells whether (t,i) is a valid trellis point
 	if (!validQ) return true;
 	return Par::LA[S.Label[t]][ian];
}


bool TrelQ(unsigned int ian, const set<unsigned int>& nodes){
	//returns true if ian is contained in nodes
	if (nodes.find(ian) == nodes.end()) return false;
	else return true;
}


void PrintFSum(Seq& S,Par& M,const unsigned int j,const unsigned int t,bool validQ){

	unsigned int n,i,fn;
	double aij;

	cout<<"a_aux("<<t<<","<<j<<"("<<"))="<<endl;
	//find all sources of j, extend each to j and sum them
	for (n=0;n<M.ArchSize[j].nsrc;n++){

		i=M.ArchRev[j][n].src;
		fn=M.ArchRev[j][n].index;
		//check if (i,t-1) is a valid trellis point
		if (!TrelQ(t-1,i,S,validQ)) continue;
		
		//fn is index of M.Arch whose target is j
		aij=M.Arch[i][fn].score;
		//aij is i->j transition score
		cout<<S.trel[t-1][i].a_scl<<" * "<<aij
			<<"\t//a_scl("<<t-1<<","<<i<<") * a("<<i
			<<","<<j<<") + ..."<<endl;
	}

	cout<<"... * "<<(S.*S.pReturn)(t,j,M)
		<<"O("<<t<<","<<j<<")"<<endl<<endl;

}


bool NotifyDeadEnd(Seq& S, unsigned int t,
				   set<unsigned int>& nodes1,
				   set<unsigned int>& nodes2){
	assert(t < S.scl.Seqlen);
	set<unsigned int>::iterator sit;
	cout<<"From position "<<t-1<<" to "<<t<<" in "<<S.scl.SeqID.data()
		<<" there are no transitions."<<endl;
	cout<<"Position "<<t-1<<" allowed nodes:"<<endl;
	for (sit = nodes1.begin(); sit != nodes1.end(); sit++) cout<<Par::SANrev[*sit]<<" ";
	cout<<endl;
	cout<<"Position "<<t<<" allowed nodes:"<<endl;
	for (sit = nodes2.begin(); sit != nodes2.end(); sit++) cout<<Par::SANrev[*sit]<<" ";
	cout<<endl;

	return false;
}	
		

bool NotifyDeadEndTerm(Seq& S, set<unsigned int>& nodes1){

	set<unsigned int>::iterator sit;
	cout<<"At end of sequence "<<S.scl.SeqID.data()
		<<" there are no transitions to END"<<endl;
	cout<<"Position "<<S.scl.Seqlen<<" (last position) allowed nodes:"<<endl;
	for (sit = nodes1.begin(); sit != nodes1.end(); sit++) cout<<Par::SANrev[*sit]<<" ";
	cout<<endl;

	return false;
}	


bool NotifyDeadEnd(Seq& S, unsigned int t, set<unsigned int>& nodes) {
	//returns false because it's used in an assert

	unsigned int T = S.scl.Seqlen;

	cout<<"At position "<<t<<" in "<<S.scl.SeqID.data()
		<<" sum of all nodes a_aux is zero. "<<endl;
	cout<<"Transition from "<<S.row[t-1].cln
		<<" to "<<S.row[t].cln<<endl;
	cout<<"Labeling window which has no legal paths:"<<endl;
	
	unsigned int beg,end,ind;
	beg = (t<15 ? 0 : t-15);
	end = (t+15 > T-1 ? T-1 : t+15);
	for (ind = beg; ind <= end; ind++) {
		if (ind == t) cout<<"->";
		cout<<S.row[ind].cln[0];
	}
	cout<<endl;
	for (ind = beg; ind <= end; ind++) {
		if (ind == t) cout<<"->";
		cout<<S.row[ind].cln[1];
	}
	cout<<endl;

	cout<<"Allowed nodes:"<<endl;
	set<unsigned int>::iterator sit;
	for (sit = nodes.begin(); sit != nodes.end(); sit++) cout<<Par::SANrev[*sit]<<" ";
	cout<<endl;

	return false;
}
