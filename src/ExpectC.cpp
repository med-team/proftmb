#include "TrainSeq.h"
#include "Par.h"
#include <cmath>

using namespace std;

//calculates E[i,c|S,M]=E[i,c|M]/P(S|M), the expected quantity
//of emitted symbol c from state i, given sequence
//S is generated from model M


void TrainSeq::ExpectationC(Par& M, bool validQ){
	//allocate
	unsigned int t,c,ian,ien;
	vector<vector<double> > C_scl(Par::NumE);

	vector<vector<double> >& Cref = (validQ ? this->C_clamp : this->C_free);
	double expectation;

	//initialize
	for (ien = 0; ien < Par::NumE; ien++)
		for (c=0;c<Par::NUMAMINO;c++){
			Cref[ien][c]=0.0;
			C_scl[ien].resize(Par::NUMAMINO);
		}

	//calculate C_k(c) for all k,c and normalize over c
	
	for (ian = 0; ian < Par::A2E.size(); ian++){ //architectural state

		ien = Par::A2E[ian];
		for (c=0; c<Par::NUMAMINO; c++){ //symbol
			C_scl[ien][c]=0.0;
			for (t=0;t<scl.Seqlen;t++)
				C_scl[ien][c] += trel[t][ian].a_scl *
					trel[t][ian].b_scl *
					this->row[t].c *
					this->Profile[t][c];
		}
	}
	
	for (ien=0; ien<Par::NumE; ien++)
		for (c=0; c<Par::NUMAMINO; c++){
			if (C_scl[ien][c] == 0.0) expectation = 0.0;
			else if (validQ) expectation = exp( log(C_scl[ien][c]) + this->scl.C_log - this->scl.P_clamp_log);
			else expectation = exp( log(C_scl[ien][c]) + this->scl.C_log - this->scl.P_free_log);
			Cref[ien][c] = expectation;
		}
}


//Because we tie emission parameters, the expectation is added.
//However, it is scaled path probabilities that are added.
