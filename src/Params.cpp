#include "Par.h"
#include "Serialize.h"
#include <fstream>

using namespace std;

void Par::Store(char *ofile){
	//store all non-static data of this model

	ofstream of(ofile);
	if (! of) {
		cerr<<"Store: Couldn't open output file "<<ofile
			<<", exiting...";
		exit(1);
	}
	
	archpair AP;
	tarsrcpair TSP;
	revpair RP;
	unsigned int UI = 0;
	double D = 0.0;

	Store2DVector(of, Arch, AP, UI, D);
	StoreVector(of, ArchSize, TSP, UI, UI);
	Store2DVector(of, ArchRev, RP, UI, UI);
	Store2DVector(of, EmitAmino, D);

	of.close();
}


void Par::Read(char *ifile){
	//read all non-static data into this model

	ifstream infile(ifile);
	if (! infile) {
		cerr<<"Store: Couldn't open output file "<<ifile
			<<", exiting...";
		exit(1);
	}
	
	archpair AP;
	tarsrcpair TSP;
	revpair RP;
	unsigned int UI = 0;
	double D = 0.0;

	Read2DVector(infile, Arch, AP, UI, D);
	ReadVector(infile, ArchSize, TSP, UI, UI);
	Read2DVector(infile, ArchRev, RP, UI, UI);
	Read2DVector(infile, EmitAmino, D);
	
	infile.close();
}


void Par::StoreStatic(char *ofile){
	//store all static data of the class

	ofstream of(ofile);
	if (! of) {
		cerr<<"StoreStatic: Couldn't open output file "<<ofile
			<<", exiting...";
		exit(1);
	}

	string S;
	unsigned int UI = 0;
	char C = 0;
	int I = 0;
	bool B = false;
	double D = 0.0;
	pair<double, double> PDD;

	StoreMap(of, AminoMap, C, UI);
	StoreMap(of, AminoMapRev, UI, C);
	StoreVector(of, A2E, I);
	StoreMap(of, SLNmap, S, UI);
	StoreMap(of, SANmap, S, UI);
	StoreMap(of, SENmap, S, UI);
	StoreMap(of, SLNrev, UI, S);
	StoreMap(of, SANrev, UI, S);
	StoreMap(of, SENrev, UI, S);
	StoreSimple(of, Begin_ian);
	StoreSimple(of, End_ian);
	StoreMap(of, L2Lmap, S, S);
	Store2DVector(of, LA, B);
	StoreSimple(of, NumE);
	StoreSimple(of, NumA);
	StoreSimple(of, NumL);
	StoreSimple(of, NumATotal);
	StoreSimple(of, NUMAMINO);
	StoreVector(of, AAComp, D);
	StoreVector(of, Zcurve, PDD, D, D);
	StoreSimple(of, Slope);
	StoreSimple(of, Intercept);
	StoreSimple(of, DelMin);
	StoreSimple(of, DelMax);
	StoreSimple(of, APlus);
	StoreSimple(of, AMinus);
	StoreSimple(of, Del);
	StoreSimple(of, Algorithm);
	StoreSimple(of, Objective);
	StoreSimple(of, Regularizer);

}


void Par::ReadStatic(char *ifile){

	string S;
	unsigned int UI = 0;
	char C = 0;
	int I = 0;
	bool B = false;
	double D = 0.0;
	pair<double, double> PDD;

	ifstream infile(ifile);
	if (! infile) {
		cerr<<"ReadStatic: Couldn't open input file "<<ifile
			<<", exiting...";
		exit(1);
	}

	ReadMap(infile, AminoMap, C, UI);
	ReadMap(infile, AminoMapRev, UI, C);
	ReadVector(infile, A2E, I);
	ReadMap(infile, SLNmap, S, UI);
	ReadMap(infile, SANmap, S, UI);
	ReadMap(infile, SENmap, S, UI);
	ReadMap(infile, SLNrev, UI, S);
	ReadMap(infile, SANrev, UI, S);
	ReadMap(infile, SENrev, UI, S);
	ReadSimple(infile, Begin_ian);
	ReadSimple(infile, End_ian);
	ReadMap(infile, L2Lmap, S, S);
	Read2DVector(infile, LA, B);
	ReadSimple(infile, NumE);
	ReadSimple(infile, NumA);
	ReadSimple(infile, NumL);
	ReadSimple(infile, NumATotal);
	ReadSimple(infile, NUMAMINO);
	ReadVector(infile, AAComp, D);
	ReadVector(infile, Zcurve, PDD, D, D);
	ReadSimple(infile, Slope);
	ReadSimple(infile, Intercept);
	ReadSimple(infile, DelMin);
	ReadSimple(infile, DelMax);
	ReadSimple(infile, APlus);
	ReadSimple(infile, AMinus);
	ReadSimple(infile, Del);
	ReadSimple(infile, Algorithm);
	ReadSimple(infile, Objective);
	ReadSimple(infile, Regularizer);

}
