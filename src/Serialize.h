#include <vector>
#include <sstream>
#include <fstream>
#include <cstdio>
#include <stdlib.h>
#include <string.h>

//using namespace std;
using std::ofstream;
using std::vector;


template <class T>
void StoreVector(ofstream& of, vector<T>& D, T dummy){
	//store a vector<T> where T has two members
	
	unsigned int a;

	for (a = 0; a < D.size(); a++){
		of<<D[a];
		if (a < D.size()-1) of<<'\t';
	}
	of<<endl<<"ENDRECORD"<<endl;
}	
	

template <class T, class M1, class M2>
void StoreVector(ofstream& of, vector<T>& D, T dummy1, M1 dummy2, M2 dummy3){
	//store a vector<T> where T has two members
	
	unsigned int a;
	M1 *m1;
	M2 *m2;
	
	for (a = 0; a < D.size(); a++){
		
		m1 = (M1 *) &D[a];
		m2 = (M2 *) (m1 + 1);
		
		of<<*m1<<','<<*m2;
		if (a < D.size()-1) of<<'\t';
	}
	of<<endl<<"ENDRECORD"<<endl;
}	
	

template <class T, class M1, class M2>
void ReadVector(ifstream& infile, vector<T>& D, T dummy1, M1 dummy2, M2 dummy3){
	//read stored data into vector<T> D

	D.resize(0);
	M1 m1;
	M2 m2;
	char c1[1000], c2[1000];
	char buf[1000];

	char *line = new char[100000];

	while (1){
		infile.getline(line,100000);
		if (strcmp(line, "ENDRECORD") == 0) break;

		istringstream iline(line);
		while (iline>>buf){
			sscanf(buf, "%[^,],%[^,]", c1, c2);
			istringstream buf2(c1);
			istringstream buf3(c2);
			buf2>>m1;
			buf3>>m2;
			D.push_back(T(m1,m2));
		}
	}
	delete [] line;
}	


	
template <class T>
void ReadVector(ifstream& infile, vector<T>& D, T dummy1){
	//read stored data into vector<T> D

	D.resize(0);
	T t;

	char buf[1000];

	char *line = new char[100000];

	while (1){
		infile.getline(line,100000);
		if (strcmp(line, "ENDRECORD") == 0) break;

		istringstream iline(line);
		while (iline>>buf){
			istringstream ibuf(buf);
			ibuf>>t;
			D.push_back(t);
		}
	}
	delete [] line;
}	


	
template<class T>
void Store2DVector(ofstream& of, vector<vector<T> >& D, T dummy){
	//store a vector<vector<T> > where T has two members
	
	unsigned int a1, a2;
	
	for (a1 = 0; a1 < D.size(); a1++){
		for (a2 = 0; a2 < D[a1].size(); a2++){
			
			of<<D[a1][a2];
			if (a2 < D[a1].size()-1) of<<'\t';
		}
		of<<endl;
	}
	of<<"ENDRECORD"<<endl;
}


template<class T, class M1, class M2>
void Store2DVector(ofstream& of, vector<vector<T> >& D, T dummy1, M1 dummy2, M2 dummy3){
	//store a vector<vector<T> > where T has two members
	
	unsigned int a1, a2;
	M1 *m1;
	M2 *m2;
	
	for (a1 = 0; a1 < D.size(); a1++){
		for (a2 = 0; a2 < D[a1].size(); a2++){
			
			m1 = (M1 *) &D[a1][a2];
			m2 = (M2 *) (m1 + 1);
			
			of<<*m1<<','<<*m2;
			if (a2 < D[a1].size()-1) of<<'\t';
		}
		of<<endl;
	}
	of<<"ENDRECORD"<<endl;
}


template <class T, class M1, class M2>
void Read2DVector(ifstream& infile, vector<vector<T> >& D,
				  T dummy1, M1 dummy2, M2 dummy3){
	//read stored data into vector<vector<T> > D

	D.resize(0);
	M1 m1;
	M2 m2;
	char c1[1000], c2[1000];
	
	char buf[1000];
	char *line = new char[100000];
	unsigned int last = 0;

	while (1){
		infile.getline(line,100000);
		istringstream iline(line);
		if (strcmp(line, "ENDRECORD") == 0) break;
		if (! infile) {
			cerr<<"Read2DVector: Reached EOF without an ENDRECORD"<<endl;
			exit(1);
		}
		D.push_back(vector<T>());
		last = D.size() - 1;

		while (iline>>buf){
			sscanf(buf, "%[^,],%[^,]", c1, c2);
			istringstream buf2(c1);
			buf2>>m1;
			istringstream buf3(c2);
			buf3>>m2;
			D[last].push_back(T(m1,m2));
		}
	}
	delete [] line;
}	


template <class T>
void Read2DVector(ifstream& infile, vector<vector<T> >& D, T dummy){
	//read stored data into vector<vector<T> > D

	D.resize(0);
	T t;
	
	char buf[1000];
	char *line = new char[100000];
	unsigned int last = 0;

	while (infile.getline(line,100000)){

		istringstream iline(line);
		if (strcmp(line, "ENDRECORD") == 0) break;
		D.push_back(vector<T>());
		last = D.size() - 1;

		while (iline>>buf){
			istringstream ibuf(buf);
			ibuf>>t;
			D[last].push_back(t);
		}
	}
	delete [] line;
}	


template <class K, class V>
void ReadMap(ifstream& infile, map<K, V>& D, K dummy1, V dummy2){
	//read stored data into map<K,V> D

	D.clear();
	K k;
	V v;

	char c1[1000], c2[1000];
	
	char buf[1000];
	char *line = new char[100000];

	while (1){
		infile.getline(line,100000);
		istringstream iline(line);
		if (strcmp(line, "ENDRECORD") == 0) break;
		if (! infile) {
			cerr<<"ReadMap: Reached EOF without an ENDRECORD"<<endl;
			exit(1);
		}

		while (iline>>buf){
			sscanf(buf, "%[^,],%[^,]", c1, c2);
			istringstream buf2(c1);
			buf2>>k;
			istringstream buf3(c2);
			buf3>>v;
			D[k] = v;
		}
	}
	delete [] line;
}

	
template<class K, class V>
void StoreMap(ofstream& of, map<K, V>& D, K dummy1, V dummy2){
	//store a vector<vector<T> > where T has two members
	
	typename map<K, V>::iterator dit;
	unsigned int ctr = 0;
	
	for (dit = D.begin(); dit != D.end(); dit++){
		of<<dit->first<<','<<dit->second;
		ctr++;
		if (ctr % 20 != 0 && ctr < D.size() - 1) of<<'\t';
		else of<<endl;
	}
	of<<"ENDRECORD"<<endl;
}


template<class S>
void StoreSimple(ofstream& of, S& D){
	//store a simple data type
	of<<D<<endl;
	of<<"ENDRECORD"<<endl;
}


template <class S>
void ReadSimple(ifstream& ifile, S& D){
	//read a simple data type
	char line[1000];
	ifile.getline(line,1000);
	istringstream iline(line);
	iline>>D;
	ifile.getline(line,1000);
	if (strcmp(line,"ENDRECORD") != 0) {
		cerr<<"ReadSimple: Missing ENDRECORD after single value"<<endl;
		exit(1);
	}
}
