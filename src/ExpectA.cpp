#include "TrainSeq.h"
#include "Par.h"
#include <cmath>
#include "HTools.h"
#include "Tools.h"


void TrainSeq::ExpectationA(Par& M, bool validQ){
	//given a labelled training sequence
	//Architecture and emission parameters, calculate 
	//the expectations E[i_ian->j_ian|S] of a transition
	//allows for a given node to have zero expected transitions

	unsigned int i_ian, j_ian, n;
	double aij;

	vector<vector<double> >& Aref = (validQ ? this->A_clamp : this->A_free);

	//initialize
	for (i_ian=0; i_ian < Par::NumATotal; i_ian++) 
		for (n = 0; n < M.ArchSize[i_ian].ntar; n++)
			Aref[i_ian][n] = 0.0;
	

	for (i_ian = 0; i_ian < Par::NumATotal; i_ian++){
		for (n = 0; n < M.ArchSize[i_ian].ntar; n++){
			j_ian = M.Arch[i_ian][n].node;
			aij = M.Arch[i_ian][n].score;
			Aref[i_ian][n] = Trans(i_ian, j_ian, aij, M, validQ);
			
		}
	}
	
}


double TrainSeq::Trans(const unsigned int s_ian,const unsigned int t_ian,double aij,Par& M, bool validQ){
	//calculates E[i->j|S, theta]  = 
	//(E_scl[S,i->j|theta] * D) / P(S|theta)
	//internally as exp { log E_scl[i->j,S|theta] + log C - log P(S|theta) }
	//to avoid underflow

	assert(s_ian != Par::Begin_ian || t_ian != Par::End_ian);

	double E_scl=0.0, expectation;
	unsigned int t, T=this->scl.Seqlen;

	if (s_ian == Par::Begin_ian)
		E_scl = trel[0][t_ian].b_scl * aij * (this->*this->pReturn)(0,t_ian,M);

	else if (t_ian == Par::End_ian)
		E_scl = trel[T-1][s_ian].a_scl * aij;
	
	else for (t=0;t<T-1;t++)
		E_scl += trel[t][s_ian].a_scl	* aij
			* trel[t+1][t_ian].b_scl * (this->*this->pReturn)(t+1,t_ian,M);


	if (E_scl == 0.0) expectation = 0.0;
	else if (validQ) expectation = exp( log(E_scl) + this->scl.C_log - this->scl.P_clamp_log);
	else expectation = exp( log(E_scl) + this->scl.C_log - this->scl.P_free_log);

	return expectation;
}



void TrainSeq::ExpectationBegin(Par& M, bool validQ){
	//calculates scaled expectations

	double scl, aij, expectation;
	unsigned int n, ian;

	for (n=0; n<M.ArchSize[Par::Begin_ian].ntar; n++){
		ian = M.Arch[Par::Begin_ian][n].node;
		aij = M.Arch[Par::Begin_ian][n].score;
		scl = trel[0][ian].b_scl = aij * (this->*this->pReturn)(0,ian,M);

		if (scl == 0.0) expectation = 0.0;
		else if (validQ) expectation = exp( log(scl) + this->scl.C_log - this->scl.P_clamp_log);
		else expectation = exp( log(scl) + this->scl.C_log - this->scl.P_free_log);

		assert(validQ || expectation != 0.0);

		if (validQ) this->A_clamp[Par::Begin_ian][n] = expectation;
		else this->A_free[Par::Begin_ian][n] = expectation;
	}
}


void TrainSeq::ExpectationEnd(Par& M, bool validQ){
	//calculates scaled expectations
	unsigned int ian, n, fn, T = this->scl.Seqlen;
	double scl, aij, expectation;

	for (n=0; n<M.ArchSize[Par::End_ian].nsrc; n++){
		ian = M.ArchRev[Par::End_ian][n].src;
		fn = M.ArchRev[Par::End_ian][n].index;
		aij = M.Arch[ian][fn].score;
		scl = trel[T-1][ian].a_scl * aij;

		if (scl == 0.0) expectation = 0.0;
		else if (validQ) expectation = exp( log(scl) + this->scl.C_log - this->scl.P_clamp_log);
		else expectation = exp( log(scl) + this->scl.C_log - this->scl.P_free_log);

		assert(validQ || (expectation != 0.0));

		if (validQ) this->A_clamp[ian][fn] = expectation;
		else this->A_free[ian][fn] = expectation;
	}
}
