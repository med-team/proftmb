#include "Load.h"
#include "Tools.h"
#include "Seq.h"
#include <sstream>
#include <iterator>

using namespace std;

extern int quiet;
typedef set<pair<string,string> > SS;

vector<TrainSeq> LoadTrainSeqs(char* seqfile, char *labelfile){
	//call Read, then iterate through the map,
	//call Seq, push_back to the vector
	vector<TrainSeq>seqs;
	set<string>emptyset;
	map<string,string>nomap;
	map<string,SS >seqdat, labdat, merged;
	map<string,SS >::iterator idat;

	try {seqdat = Seq::Read(seqfile);}
	catch (string &msg){
		cerr<<msg<<endl;
		exit(1);
	}

	try {labdat = Seq::Read(labelfile);}
	catch (string &msg){
		cerr<<msg<<endl;
		exit(1);
	}

	string id;
	for (idat = labdat.begin(); idat != labdat.end(); idat++){
		id = idat->first;
		if (seqdat.find(id) == seqdat.end()){
			cerr<<"For "<<id<<", label present in "<<labelfile<<endl
				<<" but missing from "<<seqfile<<endl;
			exit(1);
		}
		labdat[id].insert(seqdat[id].begin(), seqdat[id].end());
	}

	//check integrity of final labdat
	string dataerror;
	for (idat = labdat.begin(); idat != labdat.end(); idat++){
		dataerror = Seq::CheckDat(idat->second);
		if (dataerror.size() != 0){
			cerr<<"Sequence "<<idat->first<<" has following problem(s):"
				<<endl<<dataerror;
			exit(1);
		}
	}

	for (idat = labdat.begin(); idat != labdat.end(); idat++)
		seqs.push_back(TrainSeq(idat->second,idat->first,emptyset,nomap));

	if( !quiet )cerr<<"Loaded "<<seqfile<<endl;
	return seqs;
}


vector<Seq> LoadTestSeqs(char* seqfile){
	//call Read, then iterate through the map,
	//call Seq, push_back to the vector
	vector<Seq>seqs;
	set<string>emptyset;
	map<string,string>nomap;
	map<string,SS >seqdat;
	map<string,SS >::iterator idat;

	try {seqdat = Seq::Read(seqfile);}
	catch (string &msg){
		cerr<<msg<<endl;
		exit(1);
	}


	//check integrity of merged structure
	string dataerror;
	for (idat = seqdat.begin(); idat != seqdat.end(); idat++){
		dataerror = Seq::CheckDat(idat->second);
		if (dataerror.size() != 0){
			cerr<<"Sequence "<<idat->first<<" has following problem(s):"
				<<endl<<dataerror;
			exit(1);
		}
	}


	for (idat = seqdat.begin(); idat != seqdat.end(); idat++)
		seqs.push_back(Seq(idat->second,idat->first,emptyset,nomap));

	if( !quiet )cerr<<"Loaded "<<seqfile<<endl;
	return seqs;
}


Seq LoadOneSeq(ifstream& ifile,set<string>& posID,
			   map<string,string>& cred){
	//call ReadOne, make a Seq and return it.
	if (! ifile.good()) return Seq();
	pair<string,SS >one(Seq::ReadOne(ifile));

	string dataerror = Seq::CheckDat(one.second);

	if (dataerror.size() != 0){
		cerr<<"Sequence "<<one.first<<" has following problem(s):"<<endl;
		cerr<<dataerror;
		exit(1);
	}

	return Seq(one.second,one.first,posID,cred);
}


vector<set<string> > LoadJackknifeGroups(char *jk){
	ifstream ifile(jk);
	vector<set<string> >groups;
	char *line = new char[100000]; //reasonable cap on line length
	string id;


	if (! ifile) {
		cerr<<"LoadJackknifeGroups: Couldn't open "<<jk<<endl;
		return groups;
	}

	while (ifile.getline(line,100000)){
		istringstream sline(line);
		groups.push_back(set<string>());
		while (sline>>id) {
			groups[groups.size()-1].insert(id);
		}
		
	}
	delete line;
	if( !quiet )cerr<<"Loaded "<<jk<<endl;
	return groups;
}
