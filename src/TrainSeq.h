#ifndef _TRAINSEQ
#define _TRAINSEQ

#include "structs.h"
#include "Seq.h"
#include "Par.h"

using namespace std;

class Par;
//class Seq;

class TrainSeq : public Seq {
 public:
	vector<vector<double> >A_clamp,A_free,C_clamp,C_free;
	//expectations for clamped and free modes

	void ExpectationA(Par&, bool);
	//umbrella function, executes all following functions
	
	void ExpectationTrans(Par&, bool);
	void ExpectationBegin(Par&, bool);
	void ExpectationEnd(Par&, bool);
	
	double Trans(const uint,const uint,double,Par&, bool);
	//Helper function for ExpectationTrans
	
	void ExpectationC(Par&, bool); //expected values of emissions
	void ExpectationP(Par&); //expected values of emissions
	TrainSeq();
	TrainSeq(set<pair<string,string> >&,string,
			 set<string>&,map<string,string>&);
	void Allocate(Par&);
};

#endif
