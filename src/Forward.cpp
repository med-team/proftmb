#include "Seq.h"
#include "Par.h"
#include "HTools.h"
#include "Tools.h"
#include <cmath>
#include <iostream>
#include <algorithm>

//see Bioinformatics: The Machine Learning Approach
//appendix D, online at http://library.books24x7.com
//variable names a=alpha,b=beta,c,C,D are used as in 
//this reference

//Calculates clamped and free variables together using scaling
//factors derived from the free variables for both clamped and free

using namespace std;


double Seq::Forward(Par& M, vector<set<unsigned int> >& nodes){
	//calculates a,a_aux, a_scl, c, C by induction

	unsigned int t, T = scl.Seqlen;
	unsigned int ian, NA=Par::NumA;

	ForwardAlloc(trel, row);
	ForwardClear(trel, row);
	ForwardInit(M, trel[0], row[0], nodes[0]);
	for (t = 1; t < T; t++) {
		for (ian = 0; ian < NA; ian++)
			trel[t][ian].a_aux = AAux(M, ian, t, trel[t-1], nodes[t-1], nodes[t]);

		AScale(M, trel[t], row[t-1].C_log, row[t], nodes[t]);
		if (row[t].C_log == -HUGE_VAL) NotifyDeadEnd(*this, t, nodes[t-1], nodes[t]);

	}

	double plog = Term(M, trel[T-1], row[T-1], nodes[T-1]);

	if (plog == -HUGE_VAL) NotifyDeadEndTerm(*this, nodes[T-1]);


	CalcFinal(M);
	CalcDlog(row);

	return plog;
}


double Seq::Forward(Par& M){
	//calculates a,a_aux, a_scl, c, C by induction

	unsigned int ian, T = scl.Seqlen;

	set<unsigned int> allnodes;
	for (ian = 0; ian < Par::NumA; ian++) allnodes.insert(ian);

	vector<set<unsigned int> >nodes(T);
	fill(nodes.begin(), nodes.end(), allnodes);

	double plog = Forward(M, nodes);
	return plog;
}


void Seq::ForwardAlloc(vector<vector<mat> >& trellis,
					   vector<vec>& row){

	unsigned int t, T = scl.Seqlen, NA = Par::NumA;
	
	//resize variables
	trellis.resize(T);
	row.resize(T);
	
	for (t = 0; t < T; t++) trellis[t].resize(NA);

}				  


void Seq::ForwardClear(vector<vector<mat> >& trellis, vector<vec>& row){
	//clear values

	unsigned int ian, t;

	for (t = 0; t < row.size(); t++) row[t].init_vec();
	for (t = 0; t < trellis.size(); t++) 
		for (ian = 0; ian < trellis[t].size(); ian++) 
			trellis[t][ian]=mat();
}


void Seq::ForwardInit(Par& M,
					  vector<mat>& trellis,
					  vec& row,
					  set<unsigned int>& nodes){
	//Initializes all alpha and scale variables in 'nodes'

	unsigned int ian, n;
	double aij;

	//calculate a_aux(0,ian) for selected ian
	vector<pair<unsigned int, double> >targets =
		M.FindTargets(Par::Begin_ian, nodes);

	for (n = 0; n < targets.size(); n++){

		ian = targets[n].first;
		aij = targets[n].second;
		
		trellis[ian].a_aux = aij *(this->*this->pReturn)(0,ian,M);
		
		assert(! isnan(trellis[ian].a_aux) &&
			   ! isinf(trellis[ian].a_aux));
	}

	AScale(M, trellis, 0, row, nodes);

}


double Seq::AAux(Par& M, 
				const unsigned int t_ian,
				const unsigned int t,
				const vector<mat>& trel1,
				const set<unsigned int>& nodes1,
				const set<unsigned int>& nodes2){
						   
	//t is column of t_ian, trel1 is column before that
	//considering only valid states

	if (!TrelQ(t_ian,nodes2)) return 0.0;
	if (t_ian == Par::Begin_ian || t_ian == Par::End_ian) return 0.0;

	//we have a valid trellis point to calculate

	unsigned int n, s_ian;
	double aux = 0.0, aij;

	vector<pair<unsigned int, double> >sources = M.FindSources(t_ian, nodes1);
	for (n = 0; n < sources.size(); n++){

		s_ian = sources[n].first;
		aij = sources[n].second;

		aux += trel1[s_ian].a_scl * aij;
		
		assert(!isnan(aux) && ! isinf(aux));
	}
	
	aux *= (this->*this->pReturn)(t,t_ian,M);
	assert(! isnan(aux) && ! isinf(aux));
	return aux;
}


double Seq::Term(Par& M,
				 const vector<mat>& trel,
				 const vec& row,
				 const set<unsigned int>& nodes){
	
	//calculate the full P_aux and P_scl as the full path
	//from begin_state to end_state
	double aij, aux = 0.0, P_log;
	unsigned int ian, n;
	
	vector<pair<unsigned int, double> >sources = M.FindSources(Par::End_ian, nodes);

	for (n = 0; n < sources.size(); n++){
		ian = sources[n].first;
		aij = sources[n].second;
		aux += trel[ian].a_scl * aij;
	}

	if (aux == 0.0) P_log = -HUGE_VAL;
	else P_log = log(aux) + row.C_log;

	return P_log;
}


void Seq::CalcFinal(Par& M){


	unsigned int T = scl.Seqlen;
	scl.Pnorm_log = scl.P_free_log - (T * scl.Z_log);
	scl.null_log = CalcNulllog(M);
	scl.Pbits = scl.P_free_log - scl.null_log;

	scl.C_log = row[T-1].C_log;
}



void Seq::AScale(Par& M,
				 vector<mat>& trel,
				 double Clog1,
				 vec& row2,
				 const set<unsigned int>& nodes){
	
	//scales trellis column 'trel' but only a subset of
	//nodes in 'nodes'.  If they are all zero, the hypothesis
	//is impossible according to M.
	//Clog1 pertains to the assumed hypothesis leading up to the current position
	//row2 is the scaling factor pertaining to the subset defined by 'nodes'

	unsigned int ian, NAT = Par::NumATotal;
	
	double sum = 0.0;
	for (ian = 0; ian < NAT; ian++)
		if (TrelQ(ian, nodes)) sum += trel[ian].a_aux;
	
	row2.c = sum;
	for (ian = 0; ian < NAT; ian++)
		if (TrelQ(ian, nodes))
			trel[ian].a_scl = ((row2.c == 0.0) ? 0.0 : trel[ian].a_aux / row2.c);
	
	row2.C_log = ((row2.c == 0.0) ? -HUGE_VAL : log(row2.c) + Clog1); 
}



void Seq::CalcDlog(vector<vec>& row){

	//calculate D_log(t)=log(c(T-1))+log(c(T-2))+...log(c(t))
	unsigned int T = scl.Seqlen;
	int t;
	row[T-1].D_log = log(row[T-1].c);
	for (t = T-2; t >= 0; t--)
		row[t].D_log = row[t+1].D_log+log(row[t].c);
}


double Seq::CalcNulllog(Par& M){
	double nl=0;
	for (unsigned int t=0; t < scl.Seqlen; t++){
		nl+=log(DotP(&M.AAComp[0], &Profile[t][0],Par::NUMAMINO));
	}
	return nl;
}
