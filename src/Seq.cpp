#include "constants.h"
#include "Seq.h"
#include "Tools.h"
#include "Par.h"
#include <iomanip>
#include <sstream>
#include <cmath>
#include <cstdio>
#include <string.h>

using namespace std;

typedef set<pair<string,string> > SS;

Seq::labels Seq::lb;
Seq::ext Seq::func;

Seq::Seq (){}



Seq::Seq (SS& sdat,string id,
		  set<string>& posID,map<string,string>& cred) {
	//make this opportunistic, include threads
	//this does not initialize alpha or beta variables
	pReturn=&Seq::ReturnP;
	
	ostringstream errs;
	uint t; //time
	scl.SeqID=id;

	scl.weight = 1.0;

	string label;
	SS::iterator sit;
	for (sit = sdat.begin(); sit != sdat.end(); sit++){
		if (sit->first == lb.sq) {
			scl.Seqlen = sit->second.size();
			scl.AASeq = sit->second;
		}
		else if (sit->first == lb.wt)
			scl.weight = atof(sit->second.c_str());
	}

	if (cred.find(id)!=cred.end()) scl.ClassDesc=cred[id];
	//else cout<<"Couldn't find classdescription for "<<id<<endl;
	if (posID.find(scl.ClassDesc)==posID.end()) scl.posQ=false;
	else scl.posQ=true;

	row.resize(scl.Seqlen);
	Label.resize(scl.Seqlen);

	for (t=0;t<scl.Seqlen;t++) row[t]=vec();
	
	//resize CTable
	uint istate;
	CTable.resize(Par::NEval);
	for (istate=0;istate<Par::NEval;istate++) CTable[istate].resize(Par::NEval);
	
	//initialize label strings (sln)
	try { scl.labelQ = InitLabels(sdat); }
	catch (string& msg){
		cerr<<"Error parsing labels for "<<id<<":  "<<msg<<endl;
		exit(1);
	}

	//initialize row[t].iaa for all t
	for (t=0;t<scl.Seqlen;t++) row[t].iaa=Par::AminoMap[scl.AASeq[t]];

	InitProfile(sdat,scl.Seqlen);
}	


//load a seq without the CTable
Seq::Seq (const char *qfile,const char *id){

	pReturn=&Seq::ReturnP;

	//resize CTable
	scl.SeqID = id;
	scl.labelQ = false;

	//	unsigned int istate;
// 	CTable.resize(Par::NEval);
// 	for (istate=0;istate<Par::NEval;istate++)
// 		CTable[istate].resize(Par::NEval);

	InitProfile(qfile); //this initializes Profile, AASeq, and scl.Seqlen

	//initialize row[t].iaa for all t
	uint t; //time
	row.resize(scl.Seqlen);
	for (t=0;t<scl.Seqlen;t++) row[t]=vec();
	for (t=0;t<scl.Seqlen;t++) row[t].iaa=Par::AminoMap[scl.AASeq[t]];
}	



bool Seq::InitLabels(SS& sdat) throw (string&){

	SS::iterator sit;
	bool foundQ = false;
	for (sit = sdat.begin(); sit != sdat.end(); sit++)
		if (sit->first == lb.lb) { foundQ = true; break; }

	if (! foundQ) return false;

	string digit, label;
	unsigned int dig;

	//Parse labels into 'labels'
	vector<string> labels;
	SS::iterator it;
	for (it = sdat.begin(); it != sdat.end(); it++){
		if (it->first != lb.lb) continue;
		istringstream prline(it->second);
		prline>>digit>>label;
		dig = atoi(digit.c_str());
		if (labels.size() < dig+1) labels.resize(dig+1);
		labels[dig] = label;
	}

	ostringstream errs;

	//Check equality of label lines;
	unsigned int ndig = labels.size();
	if (ndig == 0) return false;

	for (dig = 0; dig < ndig; dig++){
		if (labels[dig].size() != scl.Seqlen){
			errs<<"Label line "<<dig<<" has length "<<labels[dig].size()
				<<" unequal to sequence length of "<<scl.Seqlen<<endl;
		}
	}
	if (errs.str().size() != 0) throw errs.str();	

	
	//Set cln, act_ian, act_rstate for row[t]
	string sln_old, sln_new;
	sln_old.resize(ndig);

	for (unsigned int t = 0; t < scl.Seqlen; t++){
		for (dig = 0; dig < ndig; dig++)
			sln_old[dig] = labels[dig][t];
		
		if (Par::L2Lmap.find(sln_old)==Par::L2Lmap.end()){
			errs<<"E18: no l2l reduction for original label "
				<<sln_old.data();
			throw errs.str();
		}

		sln_new = Par::L2Lmap[sln_old];
		row[t].cln = sln_new;
		
		if (Par::SLNmap.find(sln_new)==Par::SLNmap.end()){
			errs<<"E19: SLNmap doesn't contain "<<sln_new.data()<<".";
			throw errs.str();
		}

		Label[t] = Par::SLNmap[sln_new];

		for (unsigned int ian=0;ian<Par::NumA;ian++)
			if (Par::LA[Label[t]][ian]){
				row[t].act_ian=ian;
				break;
			}
		row[t].act_rstate = Par::ReduxDecode[row[t].act_ian];

	}
	return true;

}


void Seq::InitProfile(const char *qfile){
	//initializes the profile for the calling Seq object
	//modifies variable Profile[t][c], where t is time (position)
	//, iam=integer amino acid.  also uses Par::AminoMap[sam]=iam;
	//assumes qfile is the name of a Blast Q formatted file
	string errmsg;
	char line[1000],A[20],QueryAmino; //we expect to take in 20 amino
	int C[20]; //C[i]=composition
	ifstream q(qfile);
	if (! q){
		errmsg="InitProfile:  I couldn't open Blast Q file ";
		errmsg+=qfile;
		throw errmsg;
	}
	int i;
	//prescan for the number of valid lines, set equal to Seqlen
	q.getline(line,1000); //blank line
	q.getline(line,1000); //Last position-specific scoring matrix...
	if (strncmp(line,"Last position-specific scoring matrix computed",46)){
		errmsg=qfile;
		errmsg+=" isn't a Blast Q file";
		throw errmsg;
	}
	q.ignore(1000,'\n'); //          A R N D C Q E ...
	int T=0,t;
	while (q.getline(line,1000)){
		if (line[0]=='\0') break;
		T++;
	}
	scl.Seqlen=T;
	q.seekg(ios::beg); //reset the read position to the beginning

	//Resize Profile
	Profile.resize(scl.Seqlen);
	scl.AASeq.resize(scl.Seqlen);

	q.ignore(1000,'\n');
	q.ignore(1000,'\n');
	q.getline(line,1000);
	sscanf(line," %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c %*c\
                  %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c %c",
		   &A[0],&A[1],&A[2],&A[3],&A[4],&A[5],&A[6],&A[7],&A[8],&A[9],&A[10],
		   &A[11],&A[12],&A[13],&A[14],&A[15],&A[16],&A[17],&A[18],&A[19]);
	//
	t=0;
	while (q.getline(line,1000)){
		if (line[0]=='\0') break; //blank line signals end of the file
 		sscanf(line,"%*d %c %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d %*d\
                     %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
  			   &QueryAmino,
  			   &C[0],&C[1],&C[2],&C[3],&C[4],&C[5],&C[6],&C[7],&C[8],&C[9],&C[10],
  			   &C[11],&C[12],&C[13],&C[14],&C[15],&C[16],&C[17],&C[18],&C[19]);
		Profile[t].resize(Par::NUMAMINO);
		for (i=0;i<20;i++) Profile[t][Par::AminoMap[A[i]]]=(double)C[i];
		scl.AASeq[t]=QueryAmino;
		t++;
	}

	//normalize columns, using single sequence identity vector
	//in place of the profile column
	for (t=0;t<T;t++) 
		if (!BoolNormalize(&Profile[t][0],Par::NUMAMINO))
			Profile[t][Par::AminoMap[scl.AASeq[t]]]=1.0;

	//find sum more correctly here
	scl.S=7.40;
	
	//calculate Z_log
	double sum=0.0;
	for (i=1;i<=(int)Par::NUMAMINO;i++) sum+=log((double)i);
	scl.Z_log=Par::NUMAMINO*log(scl.S)-sum;

}


void Seq::InitProfile(SS& sdat,uint T){
	uint t,c;
	ostringstream errs;
	char num[256],amino;
	Profile.resize(T);

	for (t=0;t<T;t++) Profile[t].resize(Par::NUMAMINO);

	SS::iterator it;

	//tally all profile lines
	for (it=sdat.begin();it!=sdat.end();it++){
		if (it->first!=lb.pr) continue;
		istringstream prline(it->second); //initialize profile line for reading
		prline>>amino; //read amino symbol
		if (Par::AminoMap.find(amino)==Par::AminoMap.end()){
			errs<<"E16: Par::AminoMap doesn't contain "<<amino;
			throw errs.str();
		}

		c=Par::AminoMap[amino];
		for (t=0;t<T;t++){
			if (!(prline>>num)) {
				errs<<"E17: not enough numbers in profile.";
				throw errs.str();
			}
			assert(c<Profile[t].size());
			Profile[t][c]=atof(num);
		}
	}
	
	//normalize columns, using single sequence identity vector
	//in place of the profile column
	for (t=0;t<T;t++) 
		if (!BoolNormalize(&Profile[t][0],Par::NUMAMINO))
			Profile[t][row[t].iaa]=1.0;


	//find sum more correctly here
	scl.S=7.40;
	
	//calculate Z_log
	double sum=0.0;
	uint i;
	for (i=1;i<=Par::NUMAMINO;i++) sum+=log((double)i);
	scl.Z_log=Par::NUMAMINO*log(scl.S)-sum;

}


void Seq::InitSeq(){
	lb.id="id";
	lb.lb="label";
	lb.sq="seqres";
	lb.ts="annot2state";
	lb.wt="weight";
	lb.pr="profile";
	lb.H="Hnode";
	lb.E="Enode";
	lb.L="Lnode";
}
	

map<string,SS > Seq::Read(char* file) throw (string&){

	//load data into dat

	ifstream sf(file);
	ostringstream errs;
	if (!sf) {
		errs<<"Couldn't open "<<file;
		throw errs.str();
	}

	map<string,SS >dat;
	
	string id,label,line;

	while (sf>>id>>label){
		sf>>ws;
		getline(sf,line);
		//if (dat.find(id) == dat.end())
		//	dat[id] = SS();
		dat[id].insert(pair<string,string>(label,line));
	}
	sf.close();
	
	return dat;
}


pair<string,SS > Seq::ReadOne(ifstream& sf){
	//returns default data-structure if encounters eof
	if (! sf.good()) return pair<string,SS >();

	pair<string,SS >dat;
	string id, label ,line;

	//load data into dat

	while (sf>>id>>label){
		sf>>ws;
		getline(sf,line);
		dat.first=id;
		dat.second.insert(make_pair(label,line));
	}
		
	return dat;
}



string Seq::CheckDat(SS& dat){
	//each error accumulates a message in 'errs'
	//CheckDat returns this error, and if it has zero length
	//there is no problem

	ostringstream errs;

	//Check that all labels are known
	SS::iterator mit;
	string sq("");
	for (mit = dat.begin(); mit != dat.end(); mit++){
		if (mit->first != lb.id &&
			mit->first != lb.lb &&
			mit->first != lb.sq &&
			mit->first != lb.ts &&
			mit->first != lb.wt &&
			mit->first != lb.pr &&
			mit->first != lb.H &&
			mit->first != lb.E &&
			mit->first != lb.L)
			errs<<"Label "<<mit->first<<" isn't known."<<endl;
		if (mit->first == lb.sq) sq = mit->second;
	}

	uint T = sq.size();
	if (T == 0) {
		errs<<"Sequence is missing or has zero length."<<endl;
		throw errs.str();
	}

	//Check Profile Field
	errs<<CheckProfile(dat,T);

	return errs.str();
}
 
 
string Seq::CheckProfile(SS& dat,uint T){
	//check profile

	char num[256],amino;
	uint ctr,numc=0;
	SS::iterator it;

	ostringstream errs;

	//iterate through all profile lines
	for (it=dat.begin();it!=dat.end();it++){
		if (it->first!=lb.pr) continue;
		istringstream profile(it->second);
		profile>>amino; //read in amino label
		numc++;
		if (Par::AminoMap.find(amino)==Par::AminoMap.end()){
			errs<<"CheckProfile: Profile contains amino symbol "
			<<amino<<" not in AminoMap"<<endl;
		}
		ctr=0;
		while (profile>>num) ctr++;
		if (ctr != T){
			errs<<"CheckProfile: Profile contains "<<ctr
				<<" numbers, should be "<<T<<endl;
		}
	}
	if (numc != 0 && numc != Par::NUMAMINO){
		errs<<"CheckProfile: only "<<numc<<" symbols in profile, "
			<<"should be "<<Par::NUMAMINO<<endl;
	}

	return errs.str();

}	 


void Seq::CalcCTable(Par& M){
	//tallies the CTable variable based on the labelling
	//using the act_istate and pred_istate variables
	uint NS=Par::NEval,istate1,istate2;
	uint T=scl.Seqlen,t;

	for (istate1=0;istate1<NS;istate1++)
		for (istate2=0;istate2<NS;istate2++)
			CTable[istate1][istate2]=0;
	
	unsigned int pred, act;
	for (t=0;t<T;t++){
		assert(row[t].pred_rstate < Par::NDecode &&
			   row[t].act_rstate < Par::NDecode);
		pred = Par::ReduxEval[row[t].pred_rstate];
		act = Par::ReduxEval[row[t].act_rstate];
		CTable[pred][act]++;
	}
}




inline double Seq::ReturnA(uint t,uint ian,Par& M){
	uint ien=Par::A2E[ian];
	assert(!isnan(M.EmitAmino[ien][row[t].iaa]));
	return M.EmitAmino[ien][row[t].iaa];
}


inline double Seq::ReturnP(uint t,uint ian,Par& M){
	return DotP(&M.EmitAmino[Par::A2E[ian]][0],
				&this->Profile[t][0],Par::NUMAMINO);
}
