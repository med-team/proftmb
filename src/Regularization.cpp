#include <cmath>
#include <numeric>
#include <gsl/gsl_sf_gamma.h>
#include <algorithm>
#include "Regularization.h"
#include "Tools.h"
#include "Par.h"

using namespace std;

double NgivenAlpha(vector<double>& N, vector<double>& A){
	//calculates P(N|A,|N|), the probability of the N sample
	//given dirichlet component with alpha A
	assert(N.size() == A.size());
	double logP = 0.0;
	for (unsigned int i=0; i < N.size(); i++)
		logP += gsl_sf_lngamma(N[i]+A[i])
			-  gsl_sf_lngamma(N[i]+1.0)
			-  gsl_sf_lngamma(A[i]);
	return exp(logP);
}
			

vector<double> Dirichlet(vector<double>& N, dirichlet& M){

	unsigned int Ncomp = M.coeff.size();
	unsigned int Nalpha = N.size();
	unsigned int i, j;

	vector<double> weight(Ncomp);
	for (j = 0; j < Ncomp; j++)
		weight[j] = M.coeff[j] * NgivenAlpha(N, M.alpha[j]);
	Normalize(&weight[0], Ncomp);

	double Nnorm = accumulate(N.begin(), N.end(), 0.0);

	vector<double> Anorm(Ncomp);
	for (j = 0; j < Ncomp; j++)
		Anorm[j] = accumulate(M.alpha[j].begin(), M.alpha[j].end(), 0.0);
	
	vector<double> ptemp(Ncomp);
	vector<double> mean_post_est(Nalpha);

	for (i = 0; i < Nalpha; i++){
		for (j = 0; j < Ncomp; j++)
			ptemp[j] = (N[i] + M.alpha[j][i]) / (Nnorm + Anorm[j]);
		mean_post_est[i] = 
			inner_product(weight.begin(), weight.end(), ptemp.begin(), 0.0);
	}
	return mean_post_est;
}


void ArchPrior(vector<double>& A){
	//adjust architecture node transitions using global variables
	for (unsigned int i=0; i < A.size(); i++)
		A[i] += Par::Regularizer;
}


void EmitPrior(vector<double>& E){
	//adjust emission parameters using global variables
	E = Dirichlet(E, Par::Dirichlet);
}
