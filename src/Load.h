#ifndef _Load
#define _Load

#include "TrainSeq.h"
#include <vector>
#include <string>
#include <set>
#include <iostream>

vector<TrainSeq> LoadTrainSeqs(char *, char *);
vector<Seq> LoadTestSeqs(char *);
Seq LoadOneSeq(ifstream&);
Seq LoadOneSeq(ifstream&,set<string>&,map<string,string>&);
vector<set<string> >LoadJackknifeGroups(char *);

#endif
