#include "Seq.h"
#include "Par.h"
#include <iostream>
#include <cmath>


void Seq::CountPredStrands(){
	//counts the number of predicted beta-strands of the given
	//sequence
	//traverse this->row[t].predbetaQ, finding each switch from
	//false to true, indicating the start of a strand
	scl.npredstrands=0;

	bool prevQ=false;
	//current truth value is this->row[t].predbetaQ
	//we start out false because the first strand may start at position 0
	for (int t = 0; t<(int)scl.Seqlen; t++){
		if (row[t].predbetaQ && ! prevQ) scl.npredstrands++;
		prevQ = row[t].predbetaQ;
	}
}


void Seq::Posterior(Par& M){
	//compute multi-state reduction scores for each position t

	scl.P_free_log = Forward(M);
	Backward(M);

	//Trace forward viterbi path using multi-state reduction
	//probability as the metric
	this->LogViterbiMulti(M);

	//Traceback
	this->PosteriorTraceback(M);
 	CountPredStrands();
	if (scl.labelQ) this->CalcCTable(M);

	CalcFinal(M);
	CalcDlog(row);
	
}


vector<vector<double> > Seq::CalcReduxPosterior(Par& M){
	//calculate for each position, the reduced posterior probabilities
	//of the sequence.
	unsigned int t,s,ian;
	double total=0.0;
	vector<vector<double> >StateProbs(scl.Seqlen);
	for (t=0;t<scl.Seqlen;t++) {
		StateProbs[t].resize(Par::NDecode);
		fill(StateProbs[t].begin(), StateProbs[t].end(), 0.0);
	}

	
	for (t=0;t<scl.Seqlen;t++) {
		total = 0.0;

		//tally sums at position t
		for (ian=0;ian<Par::NumA;ian++) {
			StateProbs[t][Par::ReduxDecode[ian]]
				+=trel[t][ian].a_scl * trel[t][ian].b_scl;
			total+=trel[t][ian].a_scl * trel[t][ian].b_scl;
		}

		//normalize at position t
		for (s=0;s<Par::NDecode;s++) StateProbs[t][s]/=total;
	}
	return StateProbs;
}


void Seq::LogViterbiMulti(Par& M){
	//finds the viterbi path using ReduxDecode as the state-reduction
	//criterion, and the probability of being in one reduced state
	//as the metric
	//uses the viterbi algorithm
	unsigned int n, ian, s_ian, t_ian, t, fn;
	unsigned int T=this->scl.Seqlen, NA=Par::NumA;

	double tmpcs_log,aij_log;

	//calculate relative state probabilities for
	//each final state, store in stateprobs[pos][istate]=prob
	vector<vector<double> >StateProbs = CalcReduxPosterior(M);

	//initialization
	for (t=0;t<T;t++)
		for (ian=0;ian<NA;ian++){
			trel[t][ian].cs=0.0;
			trel[t][ian].cs_log =- HUGE_VAL;
		}
	
	for (n=0; n<M.ArchSize[Par::Begin_ian].ntar; n++){
		ian = M.Arch[Par::Begin_ian][n].node;
		aij_log = log(M.Arch[Par::Begin_ian][n].score);
		if (ian == Par::Begin_ian ||
			ian == Par::End_ian) continue;

		trel[0][ian].cs_log = aij_log + 
			log(StateProbs[0][Par::ReduxDecode[ian]]);
	}


	//extension
	for (t=1;t<T;t++) {
		for (t_ian=0;t_ian<NA;t_ian++){
			for (n=0;n<M.ArchSize[t_ian].nsrc;n++) {
				s_ian=M.ArchRev[t_ian][n].src;

				if (s_ian == Par::Begin_ian ||
					s_ian == Par::End_ian) continue;

				fn = M.ArchRev[t_ian][n].index;
				aij_log = log(M.Arch[s_ian][fn].score);

				tmpcs_log = trel[t-1][s_ian].cs_log
					+aij_log
					+log(StateProbs[t][Par::ReduxDecode[t_ian]]);

				if (trel[t][t_ian].cs_log<tmpcs_log) {
					trel[t][t_ian].cs_log=tmpcs_log;
					trel[t][t_ian].s_atom=s_ian;
				}
			}
		}
	}

	//termination
	//update the cs_log(T-1,i) for all i
	for (n=0; n < M.ArchSize[Par::End_ian].nsrc; n++){
		//find the arch score for ian->END transition if it exists
		ian = M.ArchRev[Par::End_ian][n].src;
		fn = M.ArchRev[Par::End_ian][n].index;
		aij_log = log(M.Arch[ian][fn].score);
		trel[T-1][ian].cs_log += aij_log;
	}
}


void Seq::PosteriorTraceback(Par& M){
	//traces back the viterbi path under multistate decoding
	unsigned int ian,t,T=this->scl.Seqlen,NA=Par::NumA,cur_ian,maxi=0;
	double max_log=-HUGE_VAL;
	//initialization: find top-scoring last atom
	for (ian=0;ian<NA;ian++) {
		//cout<<"log P(T-1,"<<i<<")="<<trel[T-1][i].cs_log<<endl;
		if (trel[T-1][ian].cs_log>max_log) {
			max_log=trel[T-1][ian].cs_log;
			maxi=ian;
		}
	}
	
	//record RV_log, the total 'probability' of the Reduced Viterbi
	//path.  This is not a true probability, but it is in the spirit
	//of a Viterbi path
	this->scl.RV_log=max_log;
	this->scl.RVbits=this->scl.RV_log-this->scl.null_log;

	//initialize 
	cur_ian=maxi; //initialize traceback

	//trace back by s_atom
	string curstate;
	bool strandQ = false;
	for (int it = T-1; it >= 0; it--) {
		row[it].pred_ian = cur_ian;
		row[it].pred_rstate = Par::ReduxDecode[cur_ian];
		curstate = Par::Evalrev[row[it].pred_rstate];
		strandQ = (Par::SStates.find(curstate) != Par::SStates.end());
		row[it].predbetaQ = strandQ;
		cur_ian = trel[it][cur_ian].s_atom;

	}

	//translate pred_ian's into cpn's and betaQ's using ?
	for (t=0;t<T;t++) {
		this->row[t].cpn[0]=Par::SANrev[this->row[t].pred_ian][0];
		this->row[t].cpn[1]=Par::SANrev[this->row[t].pred_ian][1];
		//warning: assigning char* to char[2]

	}
}
