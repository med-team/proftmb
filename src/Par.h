#ifndef _Par
#define _Par

#include "structs.h"
#include "Regularization.h"
#include <string>
#include <vector>
#include <map>
#include <set>
#include <utility> //for pair
#include <cmath>
#include <cassert>

enum { STD_GRAD_DESCENT, ALGORITHM_1, ALGORITHM_2, BAUM_WELCH };
enum { CML, MLFREE, MLCLAMP, DISCRIM };
enum { SINGLE, COMPLEMENT };
enum { VITERBI, APOSTERIORI, KROGH1BEST };


using namespace std;
typedef vector<vector<pair<double, double> > > STEP;

struct zcal {
	vector<double> z, cov, acc;
};

class TrainSeq;
class Seq;

class Par {
	static void InitAbet(const char*);
	static void InitConst(double,double,double, int, int, double);
	static void InitMaps(char*,char*,char*,char*);
	static void LoadL2L(char*) throw (string&);
	static void LoadL2A(char*) throw (string&);
	static void LoadA2E(char*) throw (string&);
	static void LoadConnect(char*) throw (string&);
	static void LoadRedux(char *,vector<string>&,
						  map<string, unsigned int>&,
						  vector<unsigned int>&) throw (string&);
	static void UpdateA(vector<vector<double> >&,uint,uint);
	static void UpdateA2(vector<vector<double> >&,uint,uint);

	//helper functions for construction
	void Allocate(uint,uint);
	void InitArch(double);
	void InitEmit(double);

	void BaumWelch(vector<TrainSeq>&, double, set<string>&); //iterates EM until some cutoff
	void TransExpectation(vector<TrainSeq>&, set<string>&, bool);
	void EmitExpectation(vector<TrainSeq>&, set<string>&, bool);
	void Expectations(vector<TrainSeq>&, set<string>&, bool);
	void MLFreeGradients(vector<TrainSeq>&, set<string>&);
	void MLClampGradients(vector<TrainSeq>&, set<string>&);
	void CMLGradients(vector<TrainSeq>&, set<string>&);
	void AdvanceTime();
	void UpdateEmit();
	void UpdateArch();

 public:
	void IncrComp(Seq&);
	vector<pair<unsigned int, double> > FindSources(const unsigned int, const set<unsigned int>&);
	vector<pair<unsigned int, double> > FindTargets(const unsigned int, const set<unsigned int>&);
	void ReducedViterbi(Seq&);
	Par(vector<TrainSeq>&,char*,set<string>&,bool=false);
	Par();
	pair<double, double> Update(vector<TrainSeq>&,set<string>&);
	void TallyA(vector<TrainSeq>&,set<string>&, bool);
	void TallyE(vector<TrainSeq>&,set<string>&, bool);
	double GradientStep(vector<TrainSeq>&, set<string>&);
	double BaumWelchStep(vector<TrainSeq>&, set<string>&);
	void InitializeGradientDescent();

	//from Params.cpp
	void Store(char *);
	void Read(char *);
	static void StoreStatic(char *);
	static void ReadStatic(char *);

	// i=int,s=string,d=double
	vector<vector<archpair> >Arch; // Arch[ian_src][n_for]=(node,score)
	vector<tarsrcpair>ArchSize; //ArchSize[ian]=<Arch[ian_src].size(),ArchRev[ian_targ].size()>
	vector<vector<revpair> >ArchRev; // ArchRev[ian_targ][n_rev]=pair<ian_src,n_for>
	vector<vector<double> >A_clamp,A_free,C_clamp,C_free; //A[ian][ian] and C[ien][c] convenience vars
	vector<vector<double> >EmitAmino; //EmitAmino[ien][c]=double
	vector<vector<pair<double, double> > >zArch, zEmit, delArch, delEmit; //variables for Gradient Descent training

	static map<uint,set<uint> >Connect;
	static map<char,uint> AminoMap;        // Aminomap[sam]=iam;
	static map<uint,char> AminoMapRev; //AminoMapRev[iam]=sam;
	static vector<int>A2E; //A2E[ian]=ien;
	static map<string,uint>SLNmap,SANmap,SENmap, Evalmap, Decodemap; //SLNmap[sln]=iln
	static map<uint,string>SLNrev,SANrev,SENrev; //SLNrev[iln]=sln
	static vector<string> Evalrev, Decoderev;
	static set<string>SStates; //Strand states
	static unsigned int Begin_ian, End_ian;
	static map<string,string>L2Lmap; //L2Lmap[sln_old]=sln_new
	static vector<vector<bool> >LA; //LA[iln][ian]=bool
	static vector<unsigned int> ReduxDecode, ReduxEval; //ReduxDecode[ian] = istate
	static unsigned int NDecode,NEval; //Number of decode and eval states
	static unsigned int NumE, NumA, NumL, NumATotal; //NumATotal includes BEGIN and END states
	static unsigned int NUMAMINO;
	static vector<double>AAComp; //AAComp[c]=double (counts)
	static vector<pair<double,double> >Zcurve; //Z-score (mean,sd) points at integral values
	static zcal ZCalibration; //maps z-score to <coverage, accuracy>
	static double Slope,Intercept;
	static double DelMin, DelMax, APlus, AMinus, Del; //Hyperparameters for Gradient Descent Training
	static int Algorithm, Objective, PathSelection;
	static double Regularizer;
	static bool RegularizeQ;
	static dirichlet Dirichlet;
	static void Clear();
	static void Init(char* ,char* ,char* ,char* ,char*, double, int, int, double);
	static void InitRedux(char *, char *);
	static void Init(const char*);
	static void Init(char*,const char *);
	static void InitDirichlet(char*);
	static void InitZCalibration(char*);
	static void PrintZCurve(vector<pair<double,double> >&,char *);
	static void ReadZCurve(char *) throw (string &);
	static void ReadNullFreq(char *) throw (string &);
	static double CalcDelta(pair<double, double>&, pair<double, double>&);
	static void LoadStrandStates(char *);
};

#endif
