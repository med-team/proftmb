#include "Par.h"
#include "TrainSeq.h"
#include "Tools.h"
#include "Output.h"
#include <cmath>
#include <iostream>
#include <iterator>

using namespace std;


void Par::TransExpectation(vector<TrainSeq>&tsq,set<string>& ex,bool validQ){
	//computes the sum over sequences of conditional expectations E[i->j|S,theta]
	//as 
	//of all training sequences, excluding 'ex'
	//then, normalizes by target j(for A's) or 
	//symbol c(for C's)

	unsigned int ian, n, s, D = tsq.size();
	double wt_sum;
	unsigned int ntar;

	vector<vector<double> > &Aref = (validQ ? this->A_clamp : this->A_free);

	vector<float> clampAsum(D), freeAsum(D);
	//float clampsum = 0.0, freesum = 0.0;

	//unsigned int j_ian;

	for (ian=0;ian<NumATotal;ian++){
		//compute weighted sums of A's, store in this->A,normalize over j
		ntar = this->ArchSize[ian].ntar;
		for (n = 0; n < ntar; n++){
			wt_sum=0.0;
			for (s=0;s<D;s++){
				if (ex.find(tsq[s].scl.SeqID) != ex.end()) continue;
				
				if (validQ) wt_sum += tsq[s].A_clamp[ian][n] * tsq[s].scl.weight;
				else wt_sum += tsq[s].A_free[ian][n] * tsq[s].scl.weight;
				
				clampAsum[s] += tsq[s].A_clamp[ian][n];
				freeAsum[s] += tsq[s].A_free[ian][n];
			}
			
			//wt_sum += Par::Regularizer;
			//if (wt_sum < Par::Regularizer) wt_sum += Par::Regularizer;
			Aref[ian][n] = wt_sum;
			//assert(validQ || (wt_sum != 0.0));
		}
		ArchPrior(Aref[ian]);
		BoolNormalize(&Aref[ian][0],Aref[ian].size());
		
	}
}


void Par::EmitExpectation(vector<TrainSeq>&tsq,set<string>& ex,bool validQ){

	vector<vector<double> > &Cref = (validQ ? this->C_clamp : this->C_free);
	unsigned int ien, c, s, D = tsq.size();
	double wt_sum;

	vector<double> clampEsum(D), freeEsum(D);
	//compute weighted sums of C's, store in this->C, normalize over c
	for (ien=0;ien<NumE;ien++){
		for (c=0;c<NUMAMINO;c++){
			wt_sum=0.0;
			for (s=0;s<D;s++){
				if (ex.find(tsq[s].scl.SeqID) != ex.end()) continue;

				if (validQ) wt_sum += tsq[s].C_clamp[ien][c] * tsq[s].scl.weight;
				else wt_sum += tsq[s].C_free[ien][c] * tsq[s].scl.weight;

				clampEsum[s] += tsq[s].C_clamp[ien][c];
				freeEsum[s] += tsq[s].C_free[ien][c];
			}
			//if (wt_sum < 0.001)
			//	cout<<"Cref("<<Par::SANrev[ien]<<"->"
			//		<<Par::AminoMapRev[c]<<") = "<<wt_sum<<endl;
			//assert(validQ || (wt_sum != 0.0));
			//if (wt_sum < Par::Regularizer) wt_sum = 0;
			//wt_sum += Par::Regularizer;
			Cref[ien][c] = wt_sum;
		}
		if (Par::RegularizeQ) { EmitPrior(Cref[ien]); }
		BoolNormalize(&Cref[ien][0],Cref[ien].size());
	}
}
