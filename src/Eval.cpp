#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <sstream>
#include <iterator>
#include "Eval.h"
#include "HMMEval.h"
#include "Tools.h"
#include "Sov.h"
#include <gsl/gsl_errno.h>
#include <gsl/gsl_statistics_double.h>
#include <gsl/gsl_interp.h>
#include <gsl/gsl_nan.h>

//evaluation functions for hmm's, taking the sequences as arguments

using namespace std;

double Q2(Stats& cts){
  double q2;
	q2=(cts.P+cts.N)/(cts.P+cts.O+cts.U+cts.N);
	return q2;
}


double MCC(Stats& C){
	double sum,diags,sides,mcc;
	sum=C.P+C.O+C.U+C.N;
	diags=(C.P*C.N)-(C.O*C.U);
	sides=(C.P+C.U)*(C.P+C.O)*(C.N+C.U)*(C.N+C.O);
	if (sum==0.0) {
		mcc=0.0;
		cerr<<"MCC: sum equals zero"<<endl;
	}
	else if (diags==0.0) mcc=0.0;
	else mcc=diags/sqrt(sides);
	return mcc;
}


double MCC(vector<vector<int> >& ctab,int pind){
	//calculates the MCC collapsing the ctable
	//my merging all other categories other than pind
	int Size=(int)ctab.size();
	int row,col;
	double P=0.0,O=0.0,U=0.0,N=0.0,diags,sides,sum,mcc;
	for (row=0;row<Size;row++)
		for (col=0;col<Size;col++){
			if (row==pind && col==pind) P=ctab[row][col];
			else if (row==pind && col!=pind) O+=ctab[row][col];
			else if (row!=pind && col==pind) U+=ctab[row][col];
			else N+=ctab[row][col];
		}

	sum=P+O+U+N;
	diags=(P*N)-(O*U);
	sides=(P+U)*(P+O)*(N+U)*(N+O);
	if (sum==0.0) {
		mcc=0.0;
		cerr<<"MCC: sum equals zero"<<endl;
	}
	else if (diags==0.0) mcc=0.0;
	else mcc=diags/sqrt(sides);
	return mcc;

}



double Qpctpred(vector<vector<int> >& ctab,int row){
	//merges all categories in inds
	//returns the percent of rows {inds} which are also
	//columns {inds}
	double num=0.0,den=0.0; //numerator, denominator
	uint col;
	num=ctab[row][row];
	for (col=0;col<ctab.size();col++)
		den+=ctab[row][col];

	if (num == 0.0 && den == 0.0) return 0.0;
	else return num/den;
}


double Qpctobs(vector<vector<int> >& ctab,int col){
	//merges all categories in inds
	//returns the percent of rows {inds} which are also
	//columns {inds}
	double num=0.0,den=0.0; //numerator, denominator
	uint row;
	num=ctab[col][col];
	for (row=0;row<ctab.size();row++)
		den+=ctab[row][col];

	if (num == 0.0 && den == 0.0) return 0.0;
	else return num/den;
}


double Q(vector<vector<int> >& ctab){
	//compute QN (N=ctab.size()) of a contingency table
	double num=0.0,den=0.0;
	uint row,col;
	for (row=0;row<ctab.size();row++){
		for (col=0;col<ctab.size();col++){
			if (row==col) num+=ctab[row][col]; //diagonal cell
			den+=ctab[row][col];
		}
	}
	return num/den;
}

double Q(vector<vector<float> >& ctab){
  //compute QN (N=ctab.size()) of a contingency table
  double num=0.0,den=0.0;
  uint row,col;
  for (row=0;row<ctab.size();row++){
	for (col=0;col<ctab.size();col++){
	  if (row==col) num+=ctab[row][col]; //diagonal cell
	  den+=ctab[row][col];
	}
  }
  return num/den;
}


vector<vector<float> >CTableTally(vector<TrainSeq>& TS, set<string>& ids, bool excludeQ){
	unsigned int i, j, k;
	vector<vector<float> >ct;
	ct.resize(Par::NEval);
	for (j=0; j < Par::NEval; j++) ct[j].resize(Par::NEval);
	
	for (i=0; i < TS.size(); i++){
		if (excludeQ == (ids.find(TS[i].scl.SeqID) != ids.end())) continue;
		for (j=0; j < ct.size(); j++)
			for (k=0; k < ct[j].size(); k++)
				ct[j][k] += TS[i].scl.weight * TS[i].CTable[j][k];
	}
	return ct;
}


void CTableClear(vector<vector<float> >& ct){
	unsigned int i,j;
	for (i = 0; i < ct.size(); i++){
		for (j = 0; j < ct[i].size(); j++){
			ct[i][j] = 0.0;
		}
	}
}


vector<vector<float> >CTableSum(vector<vector<float> >ct1, vector<vector<float> >ct2){

	unsigned int i, j;
	vector<vector<float> >sum(ct1.size());
	if (ct1.size() != ct2.size()) {
		cerr<<"Sizes of ctables differ.  Returning blank...";
		return sum;
	}

	for (i = 0; i < ct1.size(); i++){
		if (ct1[i].size() != ct2[i].size()) {
			cerr<<"Sizes of ctables differ.  Returning blank...";
			return sum;
		}

		sum[i].resize(ct1[i].size());
		for (j = 0; j < ct1[i].size(); j++)
			sum[i][j] = ct1[i][j] + ct2[i][j];
	}

	return sum;
}


void DecodeStats(vector<Par>& Model,
				 vector<TrainSeq>& TS,
				 vector<set<string> >& JK,
				 measure & result,
				 unsigned int alg,
				 bool testQ){

	//decodes Training sequences in TS by iterating
	//through groups in JK (jackknife) using algorithm alg
	//and if 'testQ' is true, provide results for sequences
	//absent from JK[jk].  if false, provide results for training
	//sequences (those present in JK[jk]


	unsigned int jk, seq, state;

	vector<double>Qn,SovB,SovAll;
	vector<vector<double> >Qprd(Par::NEval),Qobs(Par::NEval),MCCn(Par::NEval);
	
	for (jk = 0; jk < JK.size(); jk++){
		for (seq = 0; seq < TS.size(); seq++){
			if (testQ == (JK[jk].find(TS[seq].scl.SeqID) == JK[jk].end())) continue;
			switch (alg){
			case VITERBI:
				TS[seq].Viterbi(Model[jk]);
				break;
			case APOSTERIORI:
				TS[seq].Posterior(Model[jk]);
				break;
			case KROGH1BEST:
				TS[seq].Krogh1Best(Model[jk]);
				break;
			default:
				exit(0);
				break;
			}
	
			Qn.push_back(TS[seq].scl.weight * Q(TS[seq].CTable));
			SovB.push_back(TS[seq].scl.weight * Sov1999(TS[seq],0,'E'));
			SovAll.push_back(TS[seq].scl.weight * Sov1999(TS[seq],0,0));
			
			for (state=0; state < Par::NEval; state++){
				Qprd[state].push_back(TS[seq].scl.weight * Qpctpred(TS[seq].CTable,state));
				Qobs[state].push_back(TS[seq].scl.weight * Qpctobs(TS[seq].CTable,state));
				MCCn[state].push_back(TS[seq].scl.weight * MCC(TS[seq].CTable,state));
			}

		}

	}

	unsigned int N = Qn.size();

	result.meanQ = 100 * gsl_stats_mean(&Qn[0], 1, N);
	result.sdQ = 100 * gsl_stats_sd(&Qn[0], 1, N);
	result.meanSOVPos = 100 * gsl_stats_mean(&SovB[0], 1, N);
	result.sdSOVPos = 100 * gsl_stats_sd(&SovB[0],1, N);
	result.meanSOVALL = 100 * gsl_stats_mean(&SovAll[0],1, N);
	result.sdSOVALL = 100 * gsl_stats_sd(&SovAll[0],1, N);
	
	for (state=0;state<Par::NEval;state++){
		result.meanQpred[state] = 100*gsl_stats_mean(&Qprd[state][0],1,N);
		result.sdQpred[state] = 100*gsl_stats_sd(&Qprd[state][0],1,N);
		result.meanQobs[state] = 100*gsl_stats_mean(&Qobs[state][0],1,N);
		result.sdQobs[state] = 100*gsl_stats_sd(&Qobs[state][0],1,N);
		result.meanMCC[state] = 100*gsl_stats_mean(&MCCn[state][0],1,N);
		result.sdMCC[state] = 100*gsl_stats_sd(&MCCn[state][0],1,N);
	}
}

	
  
double Qnon(Stats& cts){return (double)cts.N/(cts.N+cts.O);}
double Qbeta(Stats& cts){return (double)cts.P/(cts.P+cts.U);}

double Qbetapred(Stats& cts){return (double)cts.P/(cts.P+cts.O);}
double Qbetaobs(Stats& cts){return (double)cts.P/(cts.P+cts.U);}

int more_eval(const void* arg1,const void* arg2){
	//compares two evaldat structures based on their
	//evalscore field
	double sco1=(*(evaldat**)arg1)->evalscore;
	double sco2=(*(evaldat**)arg2)->evalscore;
	if (sco1<sco2) return -1;
	else if (sco1 == sco2) return 0;
	else return 1;
}


int less_eval(const void* arg1,const void* arg2){
	//compares two evaldat structures based on their
	//evalscore field
	double sco1=((evaldat*)arg1)->evalscore;
	double sco2=((evaldat*)arg2)->evalscore;
	if (sco1>sco2) return -1;
	else if (sco1 == sco2) return 0;
	else return 1;
}


int less_evalp(const void* arg1,const void* arg2){
	//compares two evaldat structures based on their
	//evalscore field
	double sco1=(*(evaldat**)arg1)->evalscore;
	double sco2=(*(evaldat**)arg2)->evalscore;
	if (sco1>sco2) return -1;
	else if (sco1 == sco2) return 0;
	else return 1;
}


int more_double(const void* arg1,const void* arg2){
	double sco1=*(double*)arg1;
	double sco2=*(double*)arg2;
	if (sco1<sco2) return -1;
	else if (sco1 == sco2) return 0;
	else return 1;
}	

int less_double(const void* arg1,const void* arg2){
	double sco1=*(double*)arg1;
	double sco2=*(double*)arg2;
	if (sco1>sco2) return -1;
	else if (sco1 == sco2) return 0;
	else return 1;
}	


double ROCarea(vector<int>& tvf,int T){
	//calculates the area under a ROC curve,
	//normalized to 1.  assumes tvf is sorted so that
	//the highest score is last
	int F=tvf.size(),sum=0,ind;
	double area;
	for (ind=0;ind<F;ind++) sum+=tvf[ind];
	area=(double)sum/(double)(T*F);
	return area;
}
	

vector<roc> ROCnCurve(vector<int>&raw,int T){
	//merely converts the raw into a real roc curve
	//assumes raw is sorted ascending in .first
	int F=raw.size();
	vector<roc> curve(F);
	for (int i=0;i<F;i++)
		curve[i]=roc((double)raw[i]/(double)T,
						   (double)i/(double)F);
	return curve;
}


vector<int> ROCRaw(vector<evaldat>&sco,uint n){
	vector<evaldat*>psco(sco.size());
	for (int i=0;i<(int)sco.size();i++) psco[i]=&sco[i];
	return ROCRaw(psco,n);
}


vector<int> ROCRaw(vector<evaldat*>&sco,uint n){
	/*
	  Truncated RocRaw curve
	  In the following, 'the data' means the entire set
	  of data, falling in two classes, 'true positives'
	  and 'false positives'.  the variable 'sco' represents
	  the entire set of data.  This is a bit misleading since
	  in other derivations of ROC, the terms 'true positives'
	  and 'false positives' are relative to a moving cutoff
	  Here, the cutoff is implicit in the ranking 'i'

	  computes Altschul's formula: (1/nT) sum_i,(1..n)[t_i]
	  n=number of false positives looked at
	  T=total number of true positives in all data
	  t_i, number of true positives ranked ahead
	  of the ith false positive
	*/
	

	qsort(&sco[0],sco.size(),sizeof(sco[0]),less_evalp);

	//calculate T and F, error check
	uint ind,T=0,F;
	for (ind=0;ind<sco.size();ind++) if (sco[ind]->posQ) T++;
	F=(uint)sco.size()-T;
	if (n==0) n=F; //meaning of default value
	vector<int>t(n);

	if (n>F) {
		//cerr<<"ROCn: Warning: n is greater than F.  using n=F";
		n=F;
	}
	if (T==0) {
		cerr<<"ROCn: warning: no true positives found.\n";
		return t;
	}
	//increment if true positive is found

	//calculate t_i for each i
	uint ntrue=0,i;
	ind=0;
	for (i=0;i<n;i++){
		while (sco[ind]->posQ) { //current record is a true pos
			ntrue++;
			ind++;
		}
		t[i]=ntrue;
		ind++; //increment ind
	}

	return t;
}


template<class A> vector<A> Resample(vector<A>& dat,int N){
	//generates a vector<A> of N resamplings from dat
	vector<A>resam(N);
	int pick;
	for (uint i=0;i<dat.size();i++){
		pick=(int)floor(((double)rand()-1/(double)RAND_MAX)*dat.size());
		resam[i]=dat[pick];
	}
	return resam;
}


vector<roc> ROCResample (vector<evaldat>& wdat,double& ROC_orig,
					   double& ROC_mean,double& ROC_sd,int S){
	//resample with replacement from wdat S times,
	//generating a ROCn curve and ROCn score for each resampling,
	//returning the roc curve and score with sd for each point
	//use n=2*T, T=# of positives in resample
	vector<vector<int> >tvf_sam_fp(S);
	//vector<vector<double> >roc_fp_sam(n); //tp collections each of S points
	vector<double>rocdist(S);
	vector<int>T(S);

	//sam=sampling number, tp=# true positives,fp=# false positives
	//tvf_fp_sam[fp][sam]=tp
	//tvf_sam_fp[sam][fp]=tp
	//curve_sd[fp]=sd, tp_sd=tp std. deviation, calculated from 
	//rocdist[sam]=ROC, ROC is ROC score for particular curve
	//ROC_sd, std. deviation of roc scores
	
	//for each of S samplings, compile rocraw, store in tvf*
	//sam is the sampling run number, inum is the index number
	int sam,inum,I=wdat.size();
	vector<int>indices(I); //a vector holding the indices
	vector<evaldat*>psam(I);
	int pick;

	//create S roc curves
	for (sam=0;sam<S;sam++){
		T[sam]=0;
		for (inum=0;inum<I;inum++) {
			pick=(int)floor(((double)(rand()-1)/(double)RAND_MAX) * I);
			psam[inum]=&wdat[pick];
			if (psam[inum]->posQ) T[sam]++;
		}
		tvf_sam_fp[sam]=ROCRaw(psam,T[sam]*2); //here we use n=2 * T
		rocdist[sam]=ROCarea(tvf_sam_fp[sam],T[sam]);
	}

	//compute sd of S ROC scores
	ROC_sd=gsl_stats_sd(&rocdist[0],1,S);
	ROC_mean=gsl_stats_mean(&rocdist[0],1,S);
	//create tvf_fp_sam for easy tallying of curve_sd

// 	for (fp=0;fp<n;fp++) roc_fp_sam[fp].resize(S);
// 	for (sam=0;sam<S;sam++){
// 		//if n is the same, shouldn't T also be the same?
// 		T=tvf_sam_fp[sam][n-1]; //these are all sorted ascending
// 		for (fp=0;fp<n;fp++)
// 			roc_fp_sam[fp][sam]=(double)(tvf_sam_fp[sam][fp]+1)/(double)T;
// 	}

// 	//compute sd's of each fp in the curve
// 	for (fp=0;fp<n;fp++)
// 		curve_sd[fp]=gsl_stats_sd(&roc_fp_sam[fp][0],1,n);
	
	//calculate original ROC curve
	int T_orig=0;
	for (inum=0;inum<I;inum++) if (wdat[inum].posQ) T_orig++;
	vector<int>orig_raw=ROCRaw(wdat,T_orig*2);
	ROC_orig=ROCarea(orig_raw,T_orig);


	vector<roc>orig_curve=ROCnCurve(orig_raw,T_orig);

	return orig_curve;
}

		
double GigiIndex(double raw,uint len){
	double adj=raw-(20.0/2000.0)*(double)len;
 	cout<<"GigiIndex: raw="<<raw<<", len="<<len
 		<<", returning"<<adj<<endl;
	return adj;
}


map<string,vector<pair<double,double> > > ScoCov(vector<evaldat>& dat){
	//generates score vs. coverage as a vector of points
	//using sco.evalscore as the score

	uint i;
	string label;
	//get counts and scores of every protein in each class
	map<string,vector<double> >scores;
	for (uint i=0;i<dat.size();i++){
		if (scores.find(dat[i].cd)==scores.end())
			scores[dat[i].cd]=vector<double>();
		scores[dat[i].cd].push_back(dat[i].evalscore);
	}
	
	//sort each vector
	map<string,vector<double> >::iterator sit;
	for (sit=scores.begin();sit!=scores.end();sit++){
		label=sit->first;
		qsort(&scores[label][0],scores[label].size(),
			  sizeof(scores[label][0]),less_double); //comparison function?
	}

	//duplicate it in a Hash-of_arrays_of_pairs
	//and compute coverage
	map<string,vector<pair<double,double> > >svc;
	uint numpts;
	for (sit=scores.begin();sit!=scores.end();sit++){
		label=sit->first;
		numpts=scores[label].size();
		svc[label]=vector<pair<double,double> >(numpts);
		for (i=0;i<numpts;i++){
			svc[label][i].first=scores[label][i]; //duplicate first
			//svc[label][i].second=(double)i/(double)numpts; //compute cov
			svc[label][i].second=(double)i;
		}
	}
	return svc;
}


vector<pair<string,vector<double> > >AccCov(vector<evaldat>& dat,
											string &evalscore,
											bool descendingQ
											){
	//generates score vs. accuracy and score vs. coverage
	//using sco.evalscore as the score

	//calculate Ptotal
	int ind,Ptotal=0,p=0; //# negative samples, # Positive samples
	for (ind=0;ind<(int)dat.size();ind++) if (dat[ind].posQ) Ptotal++;

	//sort all data by evalscore
	//sort(dat.begin(),dat.end()); //need to provide operator< for this to work
	qsort(&dat[0],dat.size(),sizeof(evaldat),less_eval);
	if (! descendingQ) reverse(dat.begin(),dat.end());

	//create a skeleton data structure
	vector<pair<string,vector<double> > >acccov(4);
	acccov[0]=make_pair(evalscore,vector<double>());
	acccov[1]=make_pair(string("Accuracy"),vector<double>());
	acccov[2]=make_pair(evalscore,vector<double>());
	acccov[3]=make_pair(string("Coverage"),vector<double>());

	//traverse the sorted data, tallying the output as we go
	for (ind=0;ind<(int)dat.size();ind++){
		if (dat[ind].posQ) p++;
		acccov[0].second.push_back(dat[ind].evalscore);
		acccov[1].second.push_back((double)p/(double)(ind+1));
		acccov[2].second.push_back(dat[ind].evalscore);
		acccov[3].second.push_back((double)p/(double)Ptotal);
	}

	return acccov;
}


// lkajan: pre-1.15 behaviour
static double my_gsl_interp_eval( const gsl_interp * interp,
	const double xa[], const double ya[], double x,
	gsl_interp_accel * a )
{
	double y;
	int status;

	status = interp->type->eval (interp->state, xa, ya, interp->size, x, a, &y);

#define DISCARD_STATUS(s) if ((s) != GSL_SUCCESS) { GSL_ERROR_VAL("interpolation error", (s), GSL_NAN); }

	DISCARD_STATUS(status);

#undef DISCARD_STATUS

	return y;
}


pair<double, double> CalcAccCovFromZ(double& zscore){

	gsl_interp_accel *accel = gsl_interp_accel_alloc();
	int size = (int) Par::ZCalibration.z.size();
	gsl_interp* accp = 
		gsl_interp_alloc(gsl_interp_linear, size);	
	gsl_interp* covp = 
		gsl_interp_alloc(gsl_interp_linear, size);

	double 
		*z = &Par::ZCalibration.z[0],
		*cov = &Par::ZCalibration.cov[0],
		*acc = &Par::ZCalibration.acc[0];

	gsl_interp_init (accp, z, acc, size);
	gsl_interp_init (covp, z, cov, size);

	// lkajan: with GSL-1.15 gsl_interp_eval can no longer be used to extrapolate (gsl: interp.c:150: ERROR: interpolation error).
	int status;
	double 
		acci = my_gsl_interp_eval(accp, z, acc, zscore, accel),
		covi = my_gsl_interp_eval(covp, z, cov, zscore, accel);

	acci = acci > 100.0 ? 100.0 : acci;
	covi = covi > 100.0 ? 100.0 : covi;

	return make_pair(acci,covi);
}

								 

		

	
int MoreFirst(const void *p1,const void *p2){
	//compares two pair<double,string>'s by their .first
	//member
	pair<double,string> *pd1=(pair<double,string> *)p1;
	pair<double,string> *pd2=(pair<double,string> *)p2;
	if (pd1->first > pd2->first) return 1;
	else if (pd1->first == pd2->first) return 0;
	else return -1;
}


map<string,map<int,int> >CoverageTable(vector<pair<double,string> >&pdat){
	//produces a coverage table, rows labelled by pdat[i].second,
	//columns labelled by integer cutoff.  the entries in the table
	//are number of entries in pdat with score above the cutoff
	//in the column in their category

	int size=(int)pdat.size(),ind,col;
	
	string curcat;
	map<string,map<int,int> >table; //table to be returned

	qsort(&pdat[0],size,sizeof(pair<double,string>),&MoreFirst);
	//descending sort
	int min=(int)floor(pdat[0].first);
	int max=(int)floor(pdat[size-1].first);
	cout<<"min="<<min<<", max="<<max<<endl;
	for (ind=0;ind<size;ind++){
		curcat=pdat[ind].second;
				
		if (table.find(curcat)==table.end()){
			table[curcat]=map<int,int>();
			for (ind=min;ind<=max;ind++) {
				table[curcat][ind]=0;
				table["Total"][ind]=0;
			}
		}
		for (col=min;(double)col<pdat[ind].first;col++) {
			table[curcat][col]++;
			table["Total"][col]++;
		}
	}

	return table;
}


float RelativeEntropy(vector<float>& A, vector<float>& B) throw (char *){
	//compute the mutual information between vectors A and B
	unsigned int e;
	float re = 0.0,sum;
	bool zeroQ = false, badQ = false;

	ostringstream omsg;
	omsg<<"RelativeEntropy: ";

	if (A.size() != B.size()) throw "RelativeEntropy: sizes of vectors differ";

	//verify they are true distributions
	for (e=0,sum = 0.0; e<A.size(); e++) {
		sum += A[e];
		if (A[e] < 0.0) badQ = true;
		if (A[e] == 0.0) zeroQ = true;
	}
	if (sum - 1.0 > 0.00000001) badQ = true;

	if (badQ) {
		omsg<<"Problem with first distribution: "<<endl;
		copy(A.begin(),A.end(),ostream_iterator<float>(omsg,"\t"));
		badQ = false;
	}
	
	for (e=0,sum = 0.0; e<B.size(); e++) {
		sum += B[e];
		if (B[e] <= 0.0) badQ = true;
	}
	if (sum - 1.0 > 0.00000001) badQ = true;

	if (badQ) {
		omsg<<"Problem with second distribution: "<<endl;
		copy(A.begin(),A.end(),ostream_iterator<float>(omsg,"\t"));
		badQ = false;
	}

	if (badQ) throw omsg.str().c_str();
	if (zeroQ) return HUGE_VAL;

	//Now we have two distributions with no zeros
	for (e=0; e<A.size(); e++) re += A[e]*log(A[e]/B[e]);
	return re;
}

			
